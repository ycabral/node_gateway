!['IBM'][logo-ibm] !['Fast Shop'][logo-fast]

# API Orchestrator
[![pipeline status](https://gitlab.com/ibm-apollo/app_vendedor/node_gateway/badges/master/pipeline.svg)](https://gitlab.com/ibm-apollo/app_vendedor/node_gateway/commits/master)

## O que é
  API Gateway que mescla respostas de serviços já existentes em somente um endpoint.

## Instalação

### Pré-Requisitos
  O único pré-requisito do projeto é ter o [NodeJS](www.nodejs.org "NodeJs Official WebSite") instalado na sua máquina.
  
  Após a instalação ser concluída, passar para a próxima seção.

### Instalação dos pacotes
  Para instalar os pacotes 

  ```bash
  $ git clone https://gitlab.com/ibm-apollo/app_vendedor/node_gateway
  $ cd ./node_gateway
  $ npm install
  ```

### Prepração do ambiente
  Crie um arquivo no diretório `config` chamado `.development.env`, usando o `.env.example` como um template para as variáveis necessárias.

### Rodando a aplicação localmente
  ```bash
  $ npm run dev
  ```

## Gerar a documentação da aplicação
  ```bash
  $ npm run doc
  ```
  A documentação será gerada em `public/jsdoc`
  Iniciar a aplicação e acessar `http://localhost:9000/jsdoc/index.html` para visualizar a documentação gerada

# Publicando o NodeJS
  Para publicar a aplicação é necessário realizar os comandos abaixo:

  ```bash
  $ docker images # Visualizar qual a última versão de imagem do nodejs
  $ docker build . -t brbase/node:<VERSION> # Substituir <VERSION> pela TAG da versão, por exemplo: 1.1.0
  # Exemplo: docker build . -t brbase/node:2.5.4
  $ docker save brbase/node:<VERSION> | gzip -c > <DIRETORIO_FORA_DO_PROJETO>/brbasenode<VERSION_SEM_PONTUACAO>.tar.gz # Não salvar o .tar.gz dentro do diretório do projeto, pois ira exponenciar o tamanho do mesmo
  # docker save brbase/node:2.5.4 | gzip -c > ~/Documents/images/brbasenode254.tar.gz
  $ scp <DIRETORIO_FORA_DO_PROJETO>/brbasenode<VERSION_SEM_PONTUACAO>.tar.gz projeto@172.30.40.246:/app/docker/images
  # scp ~/Documents/images/brbasenode2.5.4.tar.gz projeto@172.30.34.246:/app/docker/images

  #Para ambiente de DEV, trocar o comando acima para:
  $ scp <DIRETORIO_FORA_DO_PROJETO>/brbasenode<VERSION_SEM_PONTUACAO>.tar.gz <SEU_LOGIN>@172.30.34.58:/app/apiVendedor/docker/images
  ```

  Acessar o servidor utilizando o comando `ssh projeto@172.30.40.246` (para DEV utilizar `ssh <SEU_LOGIN>@172.30.34.58`) e executar os seguintes comandos:

  ```bash
  $ cd /app/docker/images # Para DEV, o diretório é /app/apiVendedor/docker/images
  $ docker load -i brbasenode<VERSION_SEM_PONTUACAO>.tar.gz
  $ cd ..
  $ vi .env
  # Alterar o conteúdo da variavel NODE_IMG_TAG para o numero da <VERSION> que você criou
  $ docker images | grep -i '<VERSION>' # Busca se a sua imagem foi importada com sucesso
  $ app-restart # Executar somente se a sua imagem foi exibida no comando acima. Se possível, já faça o deploy do shopping cart se houver a necessidade. Somente um app-restart será o suficiente para publicar as duas aplicações

  # Se o comando acima não funcionar, executar conforme abaixo:
  $ cd /app/docker # Em DEV: /app/apiVendedor/docker
  $ docker-compose down && docker-compose up -d mongo microservice microservicenode


  # Visualizar se o serviço subiu com sucesso:
  $ docker logs -f microservicenode # Para sair: CTRL+C
  # Deve aparecer algo como "Express server listening on 0.0.0.0:9000 in development mode" após alguns segundos
  ```

## Rollback
  Caso seja ncessário um roll-back para a versão anterior é só alterar o conteúdo do arquivo `.env` para a imagem desejada e executar `app-restart`

# Publicando o Shopping Cart
  Para publicar a aplicação é necessário realizar os comandos abaixo:

  ```bash
  $ cd shoppingcart # Acessar a pasta shoppingcart dentro do projeto
  $ mvn install -DskipTests
  $ cd target

  # Antes de executar o comando abaixo, verificar se já existe microservice.jar
  $ cp ShoppingCart-microservice.jar microservice.jar

  # Acessar o servidor e verificar se no destino já existe um arquivo chamado microservice.jar. Caso sim, alterar o nome com o padrão microservice_<DATA_SEM_PONTUACAO>.jar
  $ scp microservice.jar projeto@172.30.40.246:/app/docker/target
  # Para DEV, alterar o comando acima para:
  $ scp microservice.jar <SEU_LOGIN>@172.30.34.58:/app/apiVendedor/docker/target
  ```

  Acessar o servidor utilizando o comando `ssh projeto@172.30.40.246` e executar os seguintes comandos:

  ```bash
  # Irá reiniciar o docker-compose e todas as aplicações
  $ app-restart # Se possível, já faça o deploy do NodeJS se houver a necessidade. Somente um app-restart será o suficiente para publicar as duas aplicações

  # Se o comando acima não funcionar, executar conforme abaixo:
  $ cd /app/docker # Em DEV: /app/apiVendedor/docker
  $ docker-compose down && docker-compose up -d mongo microservice microservicenode
  ```

[logo-ibm]: http://www.famouslogos.org/logos/ibm-logo.jpg
[logo-fast]: https://seeklogo.com/images/F/Fast_Shop-logo-8E9EB9B83D-seeklogo.com.png