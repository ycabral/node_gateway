process.env.NODE_ENV = process.env.NODE_ENV || 'development';

process.env.IP = process.env.IP || '0.0.0.0';
process.env.PORT = process.env.PORT || 9000;

const app = require('./config/express')();

app.listen( process.env.PORT, process.env.IP, () => {
  console.log(`Server listening at ${process.env.IP}:${process.env.PORT} in ${process.env.NODE_ENV} mode`);
});

module.exports = app;