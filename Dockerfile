# Set the base image to Node
FROM node:dubnium

# Provides cached layer for node_modules
ADD package.json /tmp/package.json
RUN cd /tmp && npm install --only=prod && mkdir -p /src && cp -a /tmp/node_modules /src/

# Define working directory
WORKDIR /src
ADD . /src

# Expose port
EXPOSE 9000

# Run app using pm2
CMD ["npm", "run", "start"]
