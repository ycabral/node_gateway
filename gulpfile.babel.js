const gulp = require('gulp')
    , del = require('del')
    , nodemon = require('nodemon')
    , eslint  = require('gulp-eslint')
    , plugins = require('gulp-load-plugins')();

const paths = {
  files: [
    'app/**/*.js',
    'config/**/*.js',
    './*.js'
  ],
  dist: 'dist'
};

const onServerLog = (log) => {
  console.log(plugins.util.colors.white('[') +
    plugins.util.colors.yellow('nodemon') +
    plugins.util.colors.white('] ') +
    log.message);
};

gulp.task('start', gulp.series( done => {
  process.env.NODE_ENV = process.env.NODE_ENV || 'development';
  nodemon(`-w ./app /config index.js `)
    .on('log', onServerLog);
  done();
}));

// Serve DEV Task
gulp.task('serve', gulp.series('start', done => 
  done()
));

gulp.task('clean:dist', gulp.series( done => {
  del([`${paths.dist}/!(.git*)**`], {dot: true});
  done();
}));

gulp.task('build:lint', gulp.series( done => {
  gulp.src( paths.files )
    .pipe( eslint({ useEslintrc: true }) )
    .pipe( eslint.format() )
    .pipe( eslint.failAfterError() );
  done();
}));

gulp.task('transpile:server', gulp.series( done => {
  gulp.src( paths.files )
    .pipe(plugins.sourcemaps.init())
    .pipe(plugins.babel(), {
      plugins: [
        'transform-class-properties',
        'transform-runtime'
      ]
    })
    .pipe(plugins.sourcemaps.write(), '.')
    .pipe(gulp.dest(paths.dist) );
  done();
}));

gulp.task('copy:server', gulp.series( done => {
  gulp.src( ['package.json','Dockerfile','./config/**/*', 'app/**/*.js'], {cwdbase: true} )
    .pipe( gulp.dest( paths.dist ) );
  done();
}));

// Build task
gulp.task('build', gulp.series( 'build:lint', 'clean:dist', 'transpile:server', 'copy:server', done => 
  done()
));