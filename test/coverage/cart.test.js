const addContext = require('mochawesome/addContext');
const CODVENDEDOR = 'V999998';
const CPF = '452.153.568-23'; 
const LOGIN = 'APP-TESTE';
const PASSWORD = "fast@2019";
const storeIdRetira = '01';
const termoDeBusca = 'jbl go ptob';
const paymentTerm = "78";
const quantidade = "1";
const printerId = "ws00115";
const storeIdEntrega = '87';
const storeIdEstoque = '63';
const CEP = "09570050";
const TIPO_ENTREGA = "Normal";
let accessToken;
let dadosUsuario;
let numCaixa;
let storeIdCaixa;
let tabelaVendedor;
let filialVendedor;
let cartId;
let produto;
let parcelas;
let simulaPreco;
let carrinhoFinalizado;
let cliente;
let dadosFilial;
let filialPedido;
let numPedido;
let tabelaPedido;
let sequence;
let shippingResponse;
let shippingSelect;
let dadosEntrega;

describe('Fluxo de teste - Criação de carrinho', () => {
  describe('Atividades pré-fluxo', () => {
    it('Obter token', async function() {
      const options = {
        headers: {
          'Content-type': 'application/x-www-form-urlencoded'
        },
        form: {
          'client_id': process.env.APIGEE_AUTH_CLIENT_ID,
          'client_secret': process.env.APIGEE_AUTH_CLIENT_SECRET
        },
        path: '/oauth/client_credential/accesstoken?grant_type=client_credentials',
        method: 'POST'
      };
  
      let tokenResponse = '';
      try {
        tokenResponse = await apigeeRequest( options );
      } catch (tokenErr) {
        addContext( this, {
          title: 'Body sent to service',
          value: options.body
        });

        addContext( this, {
          title: 'Response retrieved from service',
          value: tokenErr.body
        });
      };
  
      accessToken = tokenResponse.access_token;
    }).timeout( 1000 );
    
  });

  describe('Fluxo de pedido retira', () => {
    it( 'Deve realizar login no APP do Vendedor', async function() {
      const options = {
        headers: { Authorization: `Bearer ${accessToken}`, login: 'SCRIPT' },
        path: `/v1/auth/login`,
        method: 'POST',
        body: {
          login: LOGIN,
          password: PASSWORD
        }
      };

      addContext( this, {
        title: 'Request Method',
        value: options.method
      });

      addContext( this, {
        title: 'Request Path',
        value: options.path
      });

      let apigeeResponse;
      try {
        apigeeResponse = await apigeeRequest( options );
      } catch ( apigeeErr ) {
        addContext( this, {
          title: 'Body sent to service',
          value: options.body
        });

        addContext( this, {
          title: 'Error retrieved from service',
          value: apigeeErr
        });
      }

      dadosUsuario = apigeeResponse;
      
      expect( dadosUsuario ).to.not.equal( null || undefined );

      filialVendedor = apigeeResponse.storeId;
      tabelaVendedor = apigeeResponse.priceList[0];
      storeIdCaixa = apigeeResponse.cashier[0].storeId;
      numCaixa = apigeeResponse.cashier[0].number;

      //expect( response[1].statusCode ).to.be.eql( 200 );
      addContext( this, {
        title: 'Response retrieved from service',
        value: apigeeResponse
      });
    }).timeout( 1000 );

    it( 'Deve obter as condições de pagamento', async function() {
      const options = {
        headers: { Authorization: `Bearer ${accessToken}`, login: 'SCRIPT' },
        path: `/v1/price/paymentCondition`,
        method: 'GET'
      };

      addContext( this, {
        title: 'Request Method',
        value: options.method
      });

      addContext( this, {
        title: 'Request Path',
        value: options.path
      });

      let apigeeResponse;
      try {
        apigeeResponse = await apigeeRequest( options );
      } catch ( apigeeErr ) {
        addContext( this, {
          title: 'Body sent to service',
          value: options.body
        });

        addContext( this, {
          title: 'Error retrieved from service',
          value: apigeeErr
        });
      }

      expect( apigeeResponse ).to.not.equal( null || undefined );

      apigeeResponse.some( pagamento => {
        pagamento.id === paymentTerm ? parcelas = pagamento.quotes : null;
      });

      //expect( response[1].statusCode ).to.be.eql( 200 );
      addContext( this, {
        title: 'Response retrieved from service',
        value: apigeeResponse
      });
    }).timeout( 1000 );

    it( 'Deve obter as filiais', async function() {
      const options = {
        headers: { Authorization: `Bearer ${accessToken}`, login: 'SCRIPT' },
        path: `/v1/shipping/store`,
        method: 'GET'
      };

      addContext( this, {
        title: 'Request Method',
        value: options.method
      });

      addContext( this, {
        title: 'Request Path',
        value: options.path
      });

      let apigeeResponse;
      try {
        apigeeResponse = await apigeeRequest( options );
      } catch ( apigeeErr ) {
        addContext( this, {
          title: 'Body sent to service',
          value: options.body
        });

        addContext( this, {
          title: 'Error retrieved from service',
          value: apigeeErr
        });
      }

      expect( apigeeResponse ).to.not.equal( null || undefined );

      apigeeResponse.some( filial => {
        if ( filial.storeId === filialVendedor ) {
          dadosFilial = filial;
          return true;
        };
      });

      addContext( this, {
        title: 'Response retrieved from service',
        value: apigeeResponse
      });
    }).timeout( 1000 );

    it( 'Deve iniciar um pedido no shoppingCart', async function() {
      const options = {
        headers: { Authorization: `Bearer ${accessToken}`, login: 'SCRIPT' },
        path: `/v1/shoppingcart/order/create/${CODVENDEDOR}`,
        method: 'POST'
      };

      addContext( this, {
        title: 'Request Method',
        value: options.method
      });

      addContext( this, {
        title: 'Request Path',
        value: options.path
      });

      let apigeeResponse;
      try {
        apigeeResponse = await apigeeRequest( options );
      } catch ( apigeeErr ) {
        addContext( this, {
          title: 'Body sent to service',
          value: options.body
        });

        addContext( this, {
          title: 'Error retrieved from service',
          value: apigeeErr
        });
      }

      cartId = apigeeResponse.idPedido;

      expect( cartId ).to.not.equal( null || undefined );

      //expect( response[1].statusCode ).to.be.eql( 200 );
      addContext( this, {
        title: 'Response retrieved from service',
        value: apigeeResponse
      });

    }).timeout( 1000 );

    it( 'Deve realizar a busca de produto', async function() {
      const options = {
        headers: { Authorization: `Bearer ${accessToken}`, login: 'SCRIPT' },
        path: `/v1/products/chaordic/bySearchTerm?filterValue=${termoDeBusca}&pageNumber=1&priceList=${tabelaVendedor}`,
        method: 'GET'
      };

      addContext( this, {
        title: 'Request Method',
        value: options.method
      });

      addContext( this, {
        title: 'Request Path',
        value: options.path
      });

      let apigeeResponse;
      try {
        apigeeResponse = await apigeeRequest( options );
      } catch ( apigeeErr ) {
        addContext( this, {
          title: 'Body sent to service',
          value: options.body
        });

        addContext( this, {
          title: 'Error retrieved from service',
          value: apigeeErr
        });
      }

      expect( apigeeResponse ).to.not.equal( null || undefined );

      produto = apigeeResponse.products[0];

      //expect( response[1].statusCode ).to.be.eql( 200 );
      addContext( this, {
        title: 'Response retrieved from service',
        value: apigeeResponse
      });

    }).timeout( 2000 );

    it( 'Deve realizar a simulação de preço', async function() {
      const options = {
        headers: { Authorization: `Bearer ${accessToken}`, login: 'SCRIPT' },
        path: `/v1/price/simulations`,
        method: 'POST',
        body: { 
          "SKU": produto.voltage[0].sku,
          "login": LOGIN,
          "priceList": tabelaVendedor,
          "street": "A",
          "payments": [
            {
              "value": produto.voltage[0].priceOffer,
              "paymentTerm": paymentTerm,
              "input": true
            }
          ]
        }
      };

      addContext( this, {
        title: 'Request Method',
        value: options.method
      });

      addContext( this, {
        title: 'Request Path',
        value: options.path
      });

      let apigeeResponse;
      try {
        apigeeResponse = await apigeeRequest( options );
      } catch ( apigeeErr ) {
        addContext( this, {
          title: 'Body sent to service',
          value: options.body
        });

        addContext( this, {
          title: 'Error retrieved from service',
          value: apigeeErr
        });
      }

      simulaPreco = apigeeResponse;

      expect( simulaPreco ).to.not.equal( null || undefined );
      
      addContext( this, {
        title: 'Response retrieved from service',
        value: simulaPreco
      });

    }).timeout( 5000 );

    it( 'Deve verificar se o produto possui estoque disponível', async function() {
      const options = {
        headers: { Authorization: `Bearer ${accessToken}`, login: 'SCRIPT' },
        path: `/v1/availability/product/nearbygeo`,
        method: 'POST',
        body: { 
          sku: [produto.voltage[0].sku],
          geoReference: {
            lat: dadosFilial.geoLocation.lat,
            lng: dadosFilial.geoLocation.lng
          },
          priceList: tabelaVendedor
        }
      };

      addContext( this, {
        title: 'Request Method',
        value: options.method
      });

      addContext( this, {
        title: 'Request Path',
        value: options.path
      });

      let apigeeResponse;
      try {
        apigeeResponse = await apigeeRequest( options );
      } catch ( apigeeErr ) {
        addContext( this, {
          title: 'Body sent to service',
          value: options.body
        });

        addContext( this, {
          title: 'Error retrieved from service',
          value: apigeeErr
        });
      }

      expect( apigeeResponse ).to.not.equal( null || undefined );

      apigeeResponse.forEach( filial => {
        if ( filial.storeId === filialVendedor ) {
          filial.skuList[0].available.forEach( rua => {
            if ( rua.street === 'A' ) {
              expect( rua.quantity ).to.be.at.least(1, 'Produto sem estoque');
            };
          });
        };
      });
      
      addContext( this, {
        title: 'Response retrieved from service',
        value: apigeeResponse
      });

    }).timeout( 2000 );

    it( 'Deve incluir o produto no carrinho, com a simulação de preço', async function() {
      const options = {
        headers: { Authorization: `Bearer ${accessToken}`, login: 'SCRIPT' },
        path: `/v1/shoppingcart/item/add/${cartId}`,
        method: 'POST',
        body: { 
          additionalFields: [{...produto}], 
          simulationResult: {...simulaPreco}, 
          priceList: tabelaVendedor, 
          storeId: storeIdRetira,
          expeditionStoreId: storeIdRetira,
          ffmcenter: 'loja',
          interest: "0",
          itemCategory: "product",
          sku: simulaPreco.sku,
          quantity: quantidade,
          unitPrice: simulaPreco.payments[0].value
        }
      };

      addContext( this, {
        title: 'Request Method',
        value: options.method
      });

      addContext( this, {
        title: 'Request Path',
        value: options.path
      });

      let apigeeResponse;
      try {
        apigeeResponse = await apigeeRequest( options );
      } catch ( apigeeErr ) {
        addContext( this, {
          title: 'Body sent to service',
          value: options.body
        });

        addContext( this, {
          title: 'Error retrieved from service',
          value: apigeeErr
        });
      }

      expect( apigeeResponse ).to.not.equal( null || undefined );
      
      addContext( this, {
        title: 'Response retrieved from service',
        value: apigeeResponse
      });
    }).timeout( 1000 );

    it( 'Deve buscar o cliente no GAN', async function() {
      const options = {
        headers: { Authorization: `Bearer ${accessToken}`, login: 'SCRIPT' },
        path: `/v1/customer/cpf?cpf=${CPF.replace( /\D/g, '') }`,
        method: 'GET'
      };

      addContext( this, {
        title: 'Request Method',
        value: options.method
      });

      addContext( this, {
        title: 'Request Path',
        value: options.path
      });

      let apigeeResponse;
      try {
        apigeeResponse = await apigeeRequest( options );
      } catch ( apigeeErr ) {
        addContext( this, {
          title: 'Body sent to service',
          value: options.body
        });

        addContext( this, {
          title: 'Error retrieved from service',
          value: apigeeErr
        });
      }

      cliente = apigeeResponse;

      expect( cliente ).to.not.equal( null || undefined );
      
      addContext( this, {
        title: 'Response retrieved from service',
        value: cliente
      });
    }).timeout( 1000 );

    it( 'Deve atribuir um cliente ao carrinho', async function() {
      const options = {
        headers: { Authorization: `Bearer ${accessToken}`, login: 'SCRIPT' },
        path: `/v1/shoppingcart/customer/${cartId}`,
        method: 'POST',
        body: { 
          cpf: cliente.CPF,
          name: cliente.name,
          id: cliente.id
        }
      };

      addContext( this, {
        title: 'Request Method',
        value: options.method
      });

      addContext( this, {
        title: 'Request Path',
        value: options.path
      });

      let apigeeResponse;
      try {
        apigeeResponse = await apigeeRequest( options );
      } catch ( apigeeErr ) {
        addContext( this, {
          title: 'Body sent to service',
          value: options.body
        });

        addContext( this, {
          title: 'Error retrieved from service',
          value: apigeeErr
        });
      }

      expect( apigeeResponse ).to.not.equal( null || undefined );
      
      addContext( this, {
        title: 'Response retrieved from service',
        value: apigeeResponse
      });
    }).timeout( 1000 );

    it( 'Deve enviar o carrinho para o GAN', async function() {
      const options = {
        headers: { Authorization: `Bearer ${accessToken}`, login: 'SCRIPT' },
        path: `/v1/shoppingcart/checkout/${cartId}?state=F`,
        method: 'POST'
      };

      addContext( this, {
        title: 'Request Method',
        value: options.method
      });

      addContext( this, {
        title: 'Request Path',
        value: options.path
      });

      let apigeeResponse;
      try {
        apigeeResponse = await apigeeRequest( options );
      } catch ( apigeeErr ) {
        addContext( this, {
          title: 'Body sent to service',
          value: options.body
        });

        addContext( this, {
          title: 'Error retrieved from service',
          value: apigeeErr
        });
      }

      expect( apigeeResponse ).to.not.equal( null || undefined );
      
      carrinhoFinalizado = apigeeResponse;
      tabelaPedido = carrinhoFinalizado.deliveries[0].priceList;
      filialPedido = carrinhoFinalizado.deliveries[0].storeId;
      numPedido = carrinhoFinalizado.deliveries[0].orderId;

      addContext( this, {
        title: 'Response retrieved from service',
        value: apigeeResponse
      });

    }).timeout( 10000 );

    it( 'Deve obter os pagamentos pendentes do pedido', async function() {
      const options = {
        headers: { Authorization: `Bearer ${accessToken}`, login: 'SCRIPT' },
        path: `/v1/shoppingcart/getOrderPayments/${cartId}`,
        method: 'POST'
      };

      addContext( this, {
        title: 'Request Method',
        value: options.method
      });

      addContext( this, {
        title: 'Request Path',
        value: options.path
      });

      let apigeeResponse;
      try {
        apigeeResponse = await apigeeRequest( options );
      } catch ( apigeeErr ) {
        addContext( this, {
          title: 'Body sent to service',
          value: options.body
        });

        addContext( this, {
          title: 'Error retrieved from service',
          value: apigeeErr
        });
      }

      expect( apigeeResponse ).to.not.equal( null || undefined );
      
      valorRestante = apigeeResponse.remainingValue;

      addContext( this, {
        title: 'Response retrieved from service',
        value: apigeeResponse
      });

    }).timeout( 1000 );

    it( 'Deve enviar o pagamento total do pedido', async function() {
      const options = {
        headers: { Authorization: `Bearer ${accessToken}`, login: 'SCRIPT' },
        path: `/v1/shoppingcart/orderBilling/confirm`,
        method: 'POST',
        body: {
          "aquirerCode": "05",
          "cancel": false,
          "cardTypeIndex": "00000",
          "cardBin": "589916", // 6 Primeiros digitos do cartão
          "systemSource": "APP-VENDEDOR",
          "institutionName": "MAESTROCP       ",
          "paymentModalityGroup": "01",
          "cashierNumber": numCaixa,
          "cashierStoreId": storeIdCaixa,
          "conditionCode": paymentTerm,
          "customerId": carrinhoFinalizado.customer.id,
          "hostNSU": Math.floor( (Math.random() * 9999999 ) + 10000000 ).toString(),
          "sitefNSU": Math.floor( (Math.random() * 9999999 ) + 1000000 ).toString(),
          "login": LOGIN,
          "orders": [{
            "orderId": parseInt( numPedido ),
            "priceList": tabelaPedido,
            "storeId": filialPedido
          }],
          "printerId": printerId,
          "quotes": parcelas,
          "shoppingCartId": cartId,
          "totalAmount": carrinhoFinalizado.totalPayments[1].value, //valorRestante
          "transactionDateTime": new Date().toISOString()
        }
      };

      addContext( this, {
        title: 'Request Method',
        value: options.method
      });

      addContext( this, {
        title: 'Request Path',
        value: options.path
      });

      let apigeeResponse;
      try {
        apigeeResponse = await apigeeRequest( options );
      } catch ( apigeeErr ) {
        addContext( this, {
          title: 'Body sent to service',
          value: options.body
        });

        addContext( this, {
          title: 'Error retrieved from service',
          value: apigeeErr
        });
      }

      expect( apigeeResponse ).to.not.equal( null || undefined );
      
      addContext( this, {
        title: 'Response retrieved from service',
        value: apigeeResponse
      });
    }).timeout( 2000 );

    it( 'Deve gerar a nota fiscal', async function() {
      const options = {
        headers: { Authorization: `Bearer ${accessToken}`, login: 'SCRIPT' },
        path: `/v1/shoppingcart/orderBilling/invoice`,
        method: 'POST',
        body: {
          "orders": [
            {
              "storeId": filialPedido,
              "priceList": tabelaPedido,
              "orderId": parseInt( numPedido )
            }
          ],
          "shoppingCartId": cartId,
          "customerId": cliente.id,
          "cashierNumber": numCaixa,
          "cashierStoreId": storeIdCaixa,
          "login": LOGIN,
          "systemSource": "APP-VENDEDOR"
        }
      };

      addContext( this, {
        title: 'Request Method',
        value: options.method
      });

      addContext( this, {
        title: 'Request Path',
        value: options.path
      });

      let apigeeResponse;
      try {
        apigeeResponse = await apigeeRequest( options );
      } catch ( apigeeErr ) {
        addContext( this, {
          title: 'Body sent to service',
          value: options.body
        });

        addContext( this, {
          title: 'Error retrieved from service',
          value: apigeeErr
        });
      }

      expect( apigeeResponse ).to.not.equal( null || undefined );
      expect( apigeeResponse.invoice.key ).to.not.equal( null || undefined );
      
      addContext( this, {
        title: 'Response retrieved from service',
        value: apigeeResponse
      });
    }).timeout( 20000 );

  });

  /*
  describe('Fluxo de pedido entrega', () => {
    it( 'Deve realizar login no APP do Vendedor', async function() {
      const options = {
        headers: { Authorization: `Bearer ${accessToken}`, login: 'SCRIPT' },
        path: `/v1/auth/login`,
        method: 'POST',
        body: {
          login: LOGIN,
          password: PASSWORD
        }
      };

      addContext( this, {
        title: 'Request Method',
        value: options.method
      });

      addContext( this, {
        title: 'Request Path',
        value: options.path
      });

      let apigeeResponse;
      try {
        apigeeResponse = await apigeeRequest( options );
      } catch ( apigeeErr ) {
        addContext( this, {
          title: 'Body sent to service',
          value: options.body
        });

        addContext( this, {
          title: 'Error retrieved from service',
          value: apigeeErr
        });
      }

      dadosUsuario = apigeeResponse;
      
      expect( dadosUsuario ).to.not.equal( null || undefined );

      filialVendedor = apigeeResponse.storeId;
      tabelaVendedor = apigeeResponse.priceList[0];
      storeIdCaixa = apigeeResponse.cashier[0].storeId;
      numCaixa = apigeeResponse.cashier[0].number;

      //expect( response[1].statusCode ).to.be.eql( 200 );
      addContext( this, {
        title: 'Response retrieved from service',
        value: apigeeResponse
      });
    }).timeout( 1000 );

    it( 'Deve obter as condições de pagamento', async function() {
      const options = {
        headers: { Authorization: `Bearer ${accessToken}`, login: 'SCRIPT' },
        path: `/v1/price/paymentCondition`,
        method: 'GET'
      };

      addContext( this, {
        title: 'Request Method',
        value: options.method
      });

      addContext( this, {
        title: 'Request Path',
        value: options.path
      });

      let apigeeResponse;
      try {
        apigeeResponse = await apigeeRequest( options );
      } catch ( apigeeErr ) {
        addContext( this, {
          title: 'Body sent to service',
          value: options.body
        });

        addContext( this, {
          title: 'Error retrieved from service',
          value: apigeeErr
        });
      }

      expect( apigeeResponse ).to.not.equal( null || undefined );

      apigeeResponse.some( pagamento => {
        pagamento.id === paymentTerm ? parcelas = pagamento.quotes : null;
      });

      //expect( response[1].statusCode ).to.be.eql( 200 );
      addContext( this, {
        title: 'Response retrieved from service',
        value: apigeeResponse
      });
    }).timeout( 1000 );

    it( 'Deve obter as filiais', async function() {
      const options = {
        headers: { Authorization: `Bearer ${accessToken}`, login: 'SCRIPT' },
        path: `/v1/shipping/store`,
        method: 'GET'
      };

      addContext( this, {
        title: 'Request Method',
        value: options.method
      });

      addContext( this, {
        title: 'Request Path',
        value: options.path
      });

      let apigeeResponse;
      try {
        apigeeResponse = await apigeeRequest( options );
      } catch ( apigeeErr ) {
        addContext( this, {
          title: 'Body sent to service',
          value: options.body
        });

        addContext( this, {
          title: 'Error retrieved from service',
          value: apigeeErr
        });
      }

      expect( apigeeResponse ).to.not.equal( null || undefined );

      apigeeResponse.some( filial => {
        if ( filial.storeId === filialVendedor ) {
          dadosFilial = filial;
          return true;
        };
      });

      addContext( this, {
        title: 'Response retrieved from service',
        value: apigeeResponse
      });
    }).timeout( 1000 );

    it( 'Deve iniciar um pedido no shoppingCart', async function() {
      const options = {
        headers: { Authorization: `Bearer ${accessToken}`, login: 'SCRIPT' },
        path: `/v1/shoppingcart/order/create/${CODVENDEDOR}`,
        method: 'POST'
      };

      addContext( this, {
        title: 'Request Method',
        value: options.method
      });

      addContext( this, {
        title: 'Request Path',
        value: options.path
      });

      let apigeeResponse;
      try {
        apigeeResponse = await apigeeRequest( options );
      } catch ( apigeeErr ) {
        addContext( this, {
          title: 'Body sent to service',
          value: options.body
        });

        addContext( this, {
          title: 'Error retrieved from service',
          value: apigeeErr
        });
      }

      cartId = apigeeResponse.idPedido;

      expect( cartId ).to.not.equal( null || undefined );

      //expect( response[1].statusCode ).to.be.eql( 200 );
      addContext( this, {
        title: 'Response retrieved from service',
        value: apigeeResponse
      });

    }).timeout( 1000 );

    it( 'Deve realizar a busca de produto', async function() {
      const options = {
        headers: { Authorization: `Bearer ${accessToken}`, login: 'SCRIPT' },
        path: `/v1/products/chaordic/bySearchTerm?filterValue=${termoDeBusca}&pageNumber=1&priceList=${tabelaVendedor}`,
        method: 'GET'
      };

      addContext( this, {
        title: 'Request Method',
        value: options.method
      });

      addContext( this, {
        title: 'Request Path',
        value: options.path
      });

      let apigeeResponse;
      try {
        apigeeResponse = await apigeeRequest( options );
      } catch ( apigeeErr ) {
        addContext( this, {
          title: 'Body sent to service',
          value: options.body
        });

        addContext( this, {
          title: 'Error retrieved from service',
          value: apigeeErr
        });
      }

      expect( apigeeResponse ).to.not.equal( null || undefined );

      produto = apigeeResponse.products[0];

      //expect( response[1].statusCode ).to.be.eql( 200 );
      addContext( this, {
        title: 'Response retrieved from service',
        value: apigeeResponse
      });

    }).timeout( 2000 );

    it( 'Deve realizar a simulação de preço', async function() {
      const options = {
        headers: { Authorization: `Bearer ${accessToken}`, login: 'SCRIPT' },
        path: `/v1/price/simulations`,
        method: 'POST',
        body: { 
          "SKU": produto.voltage[0].sku,
          "login": LOGIN,
          "priceList": tabelaVendedor,
          "street": "A",
          "payments": [
            {
              "value": produto.voltage[0].priceOffer,
              "paymentTerm": paymentTerm,
              "input": true
            }
          ]
        }
      };

      addContext( this, {
        title: 'Request Method',
        value: options.method
      });

      addContext( this, {
        title: 'Request Path',
        value: options.path
      });

      let apigeeResponse;
      try {
        apigeeResponse = await apigeeRequest( options );
      } catch ( apigeeErr ) {
        addContext( this, {
          title: 'Body sent to service',
          value: options.body
        });

        addContext( this, {
          title: 'Error retrieved from service',
          value: apigeeErr
        });
      }

      simulaPreco = apigeeResponse;

      expect( simulaPreco ).to.not.equal( null || undefined );
      
      addContext( this, {
        title: 'Response retrieved from service',
        value: simulaPreco
      });

    }).timeout( 5000 );

    it( 'Deve verificar se o produto possui estoque disponível', async function() {
      const options = {
        headers: { Authorization: `Bearer ${accessToken}`, login: 'SCRIPT' },
        path: `/v1/availability/product/nearbygeo`,
        method: 'POST',
        body: { 
          sku: [produto.voltage[0].sku],
          geoReference: {
            lat: dadosFilial.geoLocation.lat,
            lng: dadosFilial.geoLocation.lng
          },
          priceList: tabelaVendedor
        }
      };

      addContext( this, {
        title: 'Request Method',
        value: options.method
      });

      addContext( this, {
        title: 'Request Path',
        value: options.path
      });

      let apigeeResponse;
      try {
        apigeeResponse = await apigeeRequest( options );
      } catch ( apigeeErr ) {
        addContext( this, {
          title: 'Body sent to service',
          value: options.body
        });

        addContext( this, {
          title: 'Error retrieved from service',
          value: apigeeErr
        });
      }

      expect( apigeeResponse ).to.not.equal( null || undefined );

      apigeeResponse.forEach( filial => {
        if ( filial.storeId === storeIdEstoque ) {
          filial.skuList[0].available.forEach( rua => {
            if ( rua.street === 'A' ) {
              expect( rua.quantity ).to.be.at.least(1, 'Produto sem estoque');
            };
          });
        };
      });
      
      addContext( this, {
        title: 'Response retrieved from service',
        value: apigeeResponse
      });

    }).timeout( 2000 );

    it( 'Deve incluir o produto no carrinho, com a simulação de preço', async function() {
      const options = {
        headers: { Authorization: `Bearer ${accessToken}`, login: 'SCRIPT' },
        path: `/v1/shoppingcart/item/add/${cartId}`,
        method: 'POST',
        body: { 
          additionalFields: [{...produto}], 
          simulationResult: {...simulaPreco}, 
          priceList: tabelaVendedor, 
          storeId: storeIdEstoque,
          expeditionStoreId: storeIdEntrega,
          ffmcenter: 'cd',
          interest: "0",
          itemCategory: "product",
          sku: simulaPreco.sku,
          quantity: quantidade,
          unitPrice: simulaPreco.payments[0].value
        }
      };

      addContext( this, {
        title: 'Request Method',
        value: options.method
      });

      addContext( this, {
        title: 'Request Path',
        value: options.path
      });

      let apigeeResponse;
      try {
        apigeeResponse = await apigeeRequest( options );
      } catch ( apigeeErr ) {
        addContext( this, {
          title: 'Body sent to service',
          value: options.body
        });

        addContext( this, {
          title: 'Error retrieved from service',
          value: apigeeErr
        });
      }

      expect( apigeeResponse ).to.not.equal( null || undefined );
      
      addContext( this, {
        title: 'Response retrieved from service',
        value: apigeeResponse
      });
    }).timeout( 1000 );

    it( 'Deve buscar o cliente no GAN', async function() {
      const options = {
        headers: { Authorization: `Bearer ${accessToken}`, login: 'SCRIPT' },
        path: `/v1/customer/cpf?cpf=${CPF.replace( /\D/g, '') }`,
        method: 'GET'
      };

      addContext( this, {
        title: 'Request Method',
        value: options.method
      });

      addContext( this, {
        title: 'Request Path',
        value: options.path
      });

      let apigeeResponse;
      try {
        apigeeResponse = await apigeeRequest( options );
      } catch ( apigeeErr ) {
        addContext( this, {
          title: 'Body sent to service',
          value: options.body
        });

        addContext( this, {
          title: 'Error retrieved from service',
          value: apigeeErr
        });
      }

      cliente = apigeeResponse;

      expect( cliente ).to.not.equal( null || undefined );
      
      addContext( this, {
        title: 'Response retrieved from service',
        value: cliente
      });
    }).timeout( 1000 );

    it( 'Deve atribuir um cliente ao carrinho', async function() {
      const options = {
        headers: { Authorization: `Bearer ${accessToken}`, login: 'SCRIPT' },
        path: `/v1/shoppingcart/customer/${cartId}`,
        method: 'POST',
        body: { 
          cpf: cliente.CPF,
          name: cliente.name,
          id: cliente.id
        }
      };

      addContext( this, {
        title: 'Request Method',
        value: options.method
      });

      addContext( this, {
        title: 'Request Path',
        value: options.path
      });

      let apigeeResponse;
      try {
        apigeeResponse = await apigeeRequest( options );
      } catch ( apigeeErr ) {
        addContext( this, {
          title: 'Body sent to service',
          value: options.body
        });

        addContext( this, {
          title: 'Error retrieved from service',
          value: apigeeErr
        });
      }

      expect( apigeeResponse ).to.not.equal( null || undefined );
      
      addContext( this, {
        title: 'Response retrieved from service',
        value: apigeeResponse
      });
    }).timeout( 1000 );

    it( 'Deve realizar o cálculo de frete do carrinho', async function() {

      let cepBusca;

      cliente.deliveryAddress.some( endereco => {
        if ( endereco.zipCode.replace(/\D/g, '') === CEP.replace(/\D/g, '') ) {
          cepBusca = endereco.zipCode;
          sequence = endereco.sequence;
          dadosEntrega = endereco;
          return true;
        };
      });

      expect( cepBusca ).to.not.equal( null || undefined, 'CEP nao encontrado no cadastro do cliente' );

      const options = {
        headers: { Authorization: `Bearer ${accessToken}`, login: 'SCRIPT' },
        path: `/v3/shoppingcart/shipping`,
        method: 'POST',
        body: {
          priceList: tabelaVendedor,
          expeditionStoreId: storeIdEntrega,
          storeId: storeIdEstoque,
          zipCode: cepBusca,
          paymentTerm: paymentTerm,
          products: [{
            marketPlace: false,
            marketPlaceText: "",
            sku: simulaPreco.sku,
            quantity: parseInt(quantidade),
            value: simulaPreco.payments[0].value
          }]
        }
      };

      addContext( this, {
        title: 'Request Method',
        value: options.method
      });

      addContext( this, {
        title: 'Request Path',
        value: options.path
      });

      let apigeeResponse;
      try {
        apigeeResponse = await apigeeRequest( options );
      } catch ( apigeeErr ) {
        addContext( this, {
          title: 'Body sent to service',
          value: options.body
        });

        addContext( this, {
          title: 'Error retrieved from service',
          value: apigeeErr
        });
      }

      expect( apigeeResponse ).to.not.equal( null || undefined );

      shippingResponse = apigeeResponse;

      shippingResponse.entregas[0].listaServicosLogisticos.some( servico => {
        if ( servico.servicoLogistico === TIPO_ENTREGA ) {
          shippingSelect = servico;
          return true;
        };
      });

      //expect( response[1].statusCode ).to.be.eql( 200 );
      addContext( this, {
        title: 'Response retrieved from service',
        value: apigeeResponse
      });

    }).timeout( 2000 );

    it( 'Deve definir a entrega no carrinho', async function() {
      const options = {
        headers: { Authorization: `Bearer ${accessToken}`, login: 'SCRIPT' },
        path: `/v1/shoppingcart/setShipping/${cartId}`,
        method: 'POST',
        body: {
          "deliveryCity": dadosEntrega.city,
          "deliveryState": dadosEntrega.state,
          "deliveryCost": shippingSelect.valorFrete,
          "deliveryDate": shippingSelect.dataPrevistaEntrega,
          "deliveryNameType": TIPO_ENTREGA,
          "zipCode": dadosEntrega.zipCode,
          "shippingMethod": TIPO_ENTREGA,
          "daysToDelivery": shippingSelect.prazoEmDiasUteis,
          "daysType": "bd",
          "periodDescription": TIPO_ENTREGA,
          "storeId": storeIdEntrega,
          "priceList": tabelaVendedor,
          "ffmcenter": "cd",
          "paymentTerm": paymentTerm,
          "shippingData": [shippingSelect],
        }
      };

      addContext( this, {
        title: 'Request Method',
        value: options.method
      });

      addContext( this, {
        title: 'Request Path',
        value: options.path
      });

      let apigeeResponse;
      try {
        apigeeResponse = await apigeeRequest( options );
      } catch ( apigeeErr ) {
        addContext( this, {
          title: 'Body sent to service',
          value: options.body
        });

        addContext( this, {
          title: 'Error retrieved from service',
          value: apigeeErr
        });
      }

      expect( apigeeResponse ).to.not.equal( null || undefined );

      //expect( response[1].statusCode ).to.be.eql( 200 );
      addContext( this, {
        title: 'Response retrieved from service',
        value: apigeeResponse
      });
    }).timeout( 1000 );

    it( 'Deve definir endereco de entrega no carrinho', async function() {
      const options = {
        headers: { Authorization: `Bearer ${accessToken}`, login: 'SCRIPT' },
        path: `/v1/shoppingcart/setDeliveryAddress/${cartId}`,
        method: 'POST',
        body: { sequence: sequence }
      };

      addContext( this, {
        title: 'Request Method',
        value: options.method
      });

      addContext( this, {
        title: 'Request Path',
        value: options.path
      });

      let apigeeResponse;
      try {
        apigeeResponse = await apigeeRequest( options );
      } catch ( apigeeErr ) {
        addContext( this, {
          title: 'Body sent to service',
          value: options.body
        });

        addContext( this, {
          title: 'Error retrieved from service',
          value: apigeeErr
        });
      }

      expect( apigeeResponse ).to.not.equal( null || undefined );

      //expect( response[1].statusCode ).to.be.eql( 200 );
      addContext( this, {
        title: 'Response retrieved from service',
        value: apigeeResponse
      });
    }).timeout( 1000 );

    it( 'Deve enviar o carrinho para o GAN', async function() {
      const options = {
        headers: { Authorization: `Bearer ${accessToken}`, login: 'SCRIPT' },
        path: `/v1/shoppingcart/checkout/${cartId}?state=F`,
        method: 'POST'
      };

      addContext( this, {
        title: 'Request Method',
        value: options.method
      });

      addContext( this, {
        title: 'Request Path',
        value: options.path
      });

      let apigeeResponse;
      try {
        apigeeResponse = await apigeeRequest( options );
      } catch ( apigeeErr ) {
        addContext( this, {
          title: 'Body sent to service',
          value: options.body
        });

        addContext( this, {
          title: 'Error retrieved from service',
          value: apigeeErr
        });
      }

      expect( apigeeResponse ).to.not.equal( null || undefined );
      
      carrinhoFinalizado = apigeeResponse;
      tabelaPedido = carrinhoFinalizado.deliveries[0].priceList;
      filialPedido = carrinhoFinalizado.deliveries[0].storeId;
      numPedido = carrinhoFinalizado.deliveries[0].orderId;

      addContext( this, {
        title: 'Response retrieved from service',
        value: apigeeResponse
      });

    }).timeout( 10000 );

    it( 'Deve obter os pagamentos pendentes do pedido', async function() {
      const options = {
        headers: { Authorization: `Bearer ${accessToken}`, login: 'SCRIPT' },
        path: `/v1/shoppingcart/getOrderPayments/${cartId}`,
        method: 'POST'
      };

      addContext( this, {
        title: 'Request Method',
        value: options.method
      });

      addContext( this, {
        title: 'Request Path',
        value: options.path
      });

      let apigeeResponse;
      try {
        apigeeResponse = await apigeeRequest( options );
      } catch ( apigeeErr ) {
        addContext( this, {
          title: 'Body sent to service',
          value: options.body
        });

        addContext( this, {
          title: 'Error retrieved from service',
          value: apigeeErr
        });
      }

      expect( apigeeResponse ).to.not.equal( null || undefined );
      
      valorRestante = apigeeResponse.remainingValue;

      addContext( this, {
        title: 'Response retrieved from service',
        value: apigeeResponse
      });

    }).timeout( 1000 );

    it( 'Deve enviar o pagamento total do pedido', async function() {
      const options = {
        headers: { Authorization: `Bearer ${accessToken}`, login: 'SCRIPT' },
        path: `/v1/shoppingcart/orderBilling/confirm`,
        method: 'POST',
        body: {
          "aquirerCode": "05",
          "cancel": false,
          "cardTypeIndex": "00000",
          "cardBin": "589916", // 6 Primeiros digitos do cartão
          "systemSource": "APP-VENDEDOR",
          "institutionName": "MAESTROCP       ",
          "paymentModalityGroup": "01",
          "cashierNumber": numCaixa,
          "cashierStoreId": storeIdCaixa,
          "conditionCode": paymentTerm,
          "customerId": carrinhoFinalizado.customer.id,
          "hostNSU": Math.floor( (Math.random() * 9999999 ) + 10000000 ).toString(),
          "sitefNSU": Math.floor( (Math.random() * 9999999 ) + 1000000 ).toString(),
          "login": LOGIN,
          "orders": [{
            "orderId": parseInt( numPedido ),
            "priceList": tabelaPedido,
            "storeId": filialPedido
          }],
          "printerId": printerId,
          "quotes": parcelas,
          "shoppingCartId": cartId,
          "totalAmount": carrinhoFinalizado.totalPayments[1].value, //valorRestante
          "transactionDateTime": new Date().toISOString()
        }
      };

      addContext( this, {
        title: 'Request Method',
        value: options.method
      });

      addContext( this, {
        title: 'Request Path',
        value: options.path
      });

      let apigeeResponse;
      try {
        apigeeResponse = await apigeeRequest( options );
      } catch ( apigeeErr ) {
        addContext( this, {
          title: 'Body sent to service',
          value: options.body
        });

        addContext( this, {
          title: 'Error retrieved from service',
          value: apigeeErr
        });
      }

      expect( apigeeResponse ).to.not.equal( null || undefined );
      
      addContext( this, {
        title: 'Response retrieved from service',
        value: apigeeResponse
      });
    }).timeout( 2000 );

    it( 'Deve gerar a nota fiscal', async function() {
      const options = {
        headers: { Authorization: `Bearer ${accessToken}`, login: 'SCRIPT' },
        path: `/v1/shoppingcart/orderBilling/invoice`,
        method: 'POST',
        body: {
          "orders": [
            {
              "storeId": filialPedido,
              "priceList": tabelaPedido,
              "orderId": parseInt( numPedido )
            }
          ],
          "shoppingCartId": cartId,
          "customerId": cliente.id,
          "cashierNumber": numCaixa,
          "cashierStoreId": storeIdCaixa,
          "login": LOGIN,
          "systemSource": "APP-VENDEDOR"
        }
      };

      addContext( this, {
        title: 'Request Method',
        value: options.method
      });

      addContext( this, {
        title: 'Request Path',
        value: options.path
      });

      let apigeeResponse;
      try {
        apigeeResponse = await apigeeRequest( options );
      } catch ( apigeeErr ) {
        addContext( this, {
          title: 'Body sent to service',
          value: options.body
        });

        addContext( this, {
          title: 'Error retrieved from service',
          value: apigeeErr
        });
      }

      expect( apigeeResponse ).to.not.equal( null || undefined );
      expect( apigeeResponse.invoice.key ).to.not.equal( null || undefined );
      
      addContext( this, {
        title: 'Response retrieved from service',
        value: apigeeResponse
      });
    }).timeout( 20000 );

  });
  */
});