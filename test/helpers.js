const chai = require('chai');
const supertest = require('supertest');
const app = require('../index.js');
const jsonSchema = require("chai-json-schema");
const { apigeeRequest } = require('../app/services/connections/apigee.connector');

global.app = app;
global.request = supertest(app);
global.expect = chai.expect;
global.jsonSchema = chai.use( jsonSchema );
global.apigeeRequest = apigeeRequest;