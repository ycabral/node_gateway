require('hpropagate')({
  propagateInResponses: true
});

const express       = require('express')
    , bodyParser    = require('body-parser')
    , morgan        = require('morgan')
    , Prometheus    = require('prom-client')
    , collectDefaultMetrics = Prometheus.collectDefaultMetrics
    , swaggerStats  = require('swagger-stats')
    , apiRouter     = require('../app/v1/routes/router')
    , apiRouterV2   = require('../app/v2/routes/router')
    , apiSpec       = require('../public/swagger.json');

// Probe every 5th second.    
collectDefaultMetrics({ timeout: 5000 });

module.exports = () => {
  if ( process.env.NODE_ENV !== 'kubernetes' ) {
    require('dotenv-safe').load({
      path: `${__dirname}/.${process.env.NODE_ENV}.env`,
      sample: `${__dirname}/.env.example`,
      allowEmptyValues: false
    });
  }

  const app = express();
  const sroot = process.cwd();
  const spublic = '/public/';
  const startTime = new Date();

  app.use(express.static(sroot + spublic));

  app.use( bodyParser.json() );
  app.use( require('helmet')() );
  app.use( bodyParser.urlencoded( { extended: true } ) );
  app.use( morgan('dev') );
  app.use( swaggerStats.getMiddleware({swaggerSpec: apiSpec }) );

  // Enables CORS
  app.use( ( req, res, next ) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Max-Age', '86400');
    if ( req.method === 'OPTIONS' ) {
      res.header('Access-Control-Allow-Credentials', false);
    }
    next();
  });

  // Prometheus Monitoring default metrics generator
  app.get( '/metrics', ( req, res ) => {
    res.set('Content-Type', Prometheus.register.contentType );
    res.send( Prometheus.register.metrics() );
  });

  app.use( '/*', (req, res, next) => {
    res.locals.startEpoch = Date.now();
    res.locals.login = req.headers.login;
    next();
  });
  
  app.use( '/apigw/seller'   , apiRouter   );
  app.use( '/v1'             , apiRouter   );
  app.use( '/apigw/seller/v2', apiRouterV2 );
  app.use( '/v2'             ,apiRouterV2 );

  app.use( '/*', ( req, res ) => {
    const uptime = `${new Date() - startTime}ms`;
    res.status(200).json({ startTime, uptime });
  });

  return app;
};