const cluster = require('cluster');
const cpus = process.env.CPUS || 6;

const express = require('express');
const metricsServer = express();
const AggregatorRegistry = require('prom-client').AggregatorRegistry;
const aggregatorRegistry = new AggregatorRegistry();

if ( cluster.isMaster ) {
  for ( let i = 1; i <= cpus; i++ ) {
    cluster.fork();
  }

  cluster.on('exit', () => {
    cluster.fork();
  });

  metricsServer.get('/cluster_metrics', (req, res) => {
		aggregatorRegistry.clusterMetrics((err, metrics) => {
			if (err) console.log(err);
			res.set('Content-Type', aggregatorRegistry.contentType);
			res.send(metrics);
		});
	});

	metricsServer.listen(9001);
	console.log('Cluster metrics server listening to %s, metrics exposed on /cluster_metrics', 9001);
} else { 
  require('./index');
}