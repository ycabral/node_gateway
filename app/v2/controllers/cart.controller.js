const { handleError, handleSuccess } = require( '../../services/helpers/response.handler' )
    , { CART_CLOSED } = require('../../services/helpers/cart.handler')
    , { obtemItensMarketplace, obtemItensFastShop } = require('../../services/helpers/products.handler')
    , { ganRequest, cartRequest, adminRequest } = require('../../services/connections/connectors')
    , { submitOrder, customerSync, validateCustomer } = require('./../../v1/controllers/marketPlace.controller');

// Métodos de busca de carrinho
const _get = async ( req, res ) => {
  const getCart = {
    path: `/get/${req.params.cartId}`,
    method: 'GET',
    version: 'v2'
  };

  try {
    const cartResponse = await cartRequest( getCart );
    handleSuccess( req, res, cartResponse );
  } catch ( cartError ) {
    handleError( req, res, cartError );
  };
};

const _getBySeller = async ( req, res ) => {
  const getCart = {
    path: `/findOrdersSeller/${req.params.sellerId}`,
    method: 'GET',
    version: 'v2'
  };

  try {
    const cartResponse = await cartRequest( getCart );
    handleSuccess( req, res, cartResponse );
  } catch ( cartError ) {
    handleError( req, res, cartError );
  };
};

const _getBySellerStatus = async ( req, res ) => {
  const getCart = {
    path: `/findBySellerIdAndStatus/${req.params.sellerId}/${req.params.status}/`,
    method: 'GET',
    version: 'v2'
  };

  try {
    const cartResponse = await cartRequest( getCart );
    handleSuccess( req, res, cartResponse );
  } catch ( cartError ) {
    handleError( req, res, cartError );
  };
};

const _getBySellerStatusState = async ( req, res ) => {
  const getCart = {
    path: `/findBySellerIdAndStatusAndState/${req.params.sellerId}/${req.params.status}/${req.params.state}`,
    method: 'GET',
    version: 'v2'
  };

  try {
    const cartResponse = await cartRequest( getCart );
    handleSuccess( req, res, cartResponse );
  } catch ( cartError ) {
    handleError( req, res, cartError );
  };
};

const _getByCustomer = async ( req, res ) => {
  const getCart = {
    path: `/findOrdersCustomerCpf/${req.params.document}`,
    method: 'GET',
    version: 'v2'
  };

  try {
    const cartResponse = await cartRequest( getCart );
    handleSuccess( req, res, cartResponse );
  } catch ( cartError ) {
    handleError( req, res, cartError );
  };
};

const _checkout = async ( req, res ) => {
  const cartId = req.params.cartId || req.body.shoppingCartId;
  const cartBody = res.locals.cartBody;

  // Obtém itens para realizar a reserva
  const fastShopItems = obtemItensFastShop( cartBody.deliveries[0].orderItems, 1 );
  const marketPlaceItems = obtemItensMarketplace( cartBody.deliveries[0].orderItems, 1 );

  // Sprint 016 - Habilitar Pagamento remoto
  let isRemotePayment;
  let orderId;

  // Se possuir mais de um item 
  if ( fastShopItems.length > 0 ) {

    // Sprint 016 - Habilitar Pagamento remoto
    fastShopItems.some( item => {
      if ( item.isRemotePayment ) {
        isRemotePayment = true;
        orderId = cartBody.deliveries[0].orderId;
        return true;
      };
    });

    if ( !fastShopItems.some( item => item.isWeddingGift ) ) {
      // Refaz a reserva dos itens no GAN antes de enviar o pedido.
      const ganReserveOptions = {
        body: {
          header: { function: "postReservation", login: res.locals.login },
          body: {
            storeId: cartBody.deliveries[0].storeId,
            priceList: cartBody.deliveries[0].priceList,
            orderId: !isRemotePayment ? cartId : `AZ_${orderId}`,
            salesMan: cartBody.salesMan,
            items: fastShopItems
          }
        },
        method: "POST"
      };
    
      try {
        await ganRequest( ganReserveOptions );
      } catch ( reserveError ) {
        return handleError( req, res, reserveError );
      };
    };
  
    // Um pedido sem endereço de entrega é tratado como retirada em loja. 
    if ( !(cartBody.deliveryAddress instanceof Object) || Object.keys( cartBody.deliveryAddress ).length === 0 ) {
      cartBody.deliveryAddress = {};
      cartBody.deliveryAddress.withdraw = true;
    };
  
    if ( !isRemotePayment ) {
      // Envia o pedido para o GAN
      const postOrderOptions = {
        body: {
          header: { 
            function: "postOrder", 
            brand: process.env.MQ_BRAND_ORDER, 
            login: res.locals.login, 
            version: 'v2',
            systemSource: "APP-VENDEDOR",
            id: cartBody.id
          },
          body: cartBody
        },
        method: "POST",
        timeout: process.env.GAN_TIMEOUT || 5000
      };

      let ganOrder; // Objeto contendo o retorno do GAN
      try {
        ganOrder = await ganRequest( postOrderOptions );
      } catch ( postOrderError ) {
        return handleError( req, res, postOrderError );
      };
    
      // Estava retornando um array de pedidos ao finalizar
      const getCart = {
        path: `/get/${req.params.cartId}`,
        method: 'GET',
        version: 'v2'
      };
    
      try {
        const cartResponse = await cartRequest( getCart );
        cartResponse.status.state = "M";
        cartResponse.status.value = "processing-order";
        cartResponse.status.description = "Pedido em processamento";
        
        return handleSuccess( req, res, cartResponse );
      } catch ( cartError ) {
        return handleError( req, res, cartError );
      };
    } else {
      // tenta fazer o CustomerSync
      try {
        await validateCustomer( cartBody );
        customerSync( cartBody );
      } catch ( error ) {
        return handleError( req, res, error );
      };

      let submitOrderResponse;
      try { 
        submitOrderResponse = await submitOrder( cartBody, 1 );
      } catch ( submitOrderError ) {
        return handleError( req, res, submitOrderError );
      }

      const setLink = {
        path: `/setLink/${cartId}`,
        method: 'POST',
        body: { payLink: submitOrderResponse.PayLink }
      };

      try {
        const linkResponse = await cartRequest( setLink );
      } catch ( linkError ) {
        console.error( `Falha ao atribuir link ao carrinho ${cartId}. ${submitOrderResponse.PayLink}` );
      };

      const emailOptions = {
        method: 'POST',
        from: 'EMAIL',
        body: {
          idOrder: orderId
        }
      };

      try {
        adminRequest( emailOptions );
      } catch ( emailError ) {
        handleError( req, res, emailError );
      };

      const cartOptions = {
        path: `/setStatus/${req.params.cartId}/${CART_CLOSED}/${req.query.state}`,
        method: 'POST',
        body: {
          priceList: cartBody.deliveries[0].priceList,
          storeId: cartBody.deliveries[0].storeId,
          ffmcenter: cartBody.deliveries[0].ffmcenter,
          orderId: orderId,
          expeditionStoreId: cartBody.deliveries[0].expeditionStoreId
        }
      };    

      try {
        const cartBody = await cartRequest( cartOptions );
        return handleSuccess( req, res, cartBody );
      } catch ( setStatusError ) {
        return handleError( req, res, setStatusError );
      }
    };

  }

  if ( marketPlaceItems.length > 0 ) {
    
    // tenta fazer o CustomerSync
    try {
      await validateCustomer( cartBody );
      customerSync( cartBody );
    } catch ( error ) {
      return handleError( req, res, error );
    };

    // Envia pedido para VTEX através do Barramento (ESB)
    let submitOrderResponse;
    try { 
      submitOrderResponse = await submitOrder( cartBody );
    } catch ( submitOrderError ) {
      return handleError( req, res, submitOrderError );
    }

    const cartOptions = {
      path: `/setStatus/${req.params.cartId}/${CART_CLOSED}/${req.query.state}`,
      method: 'POST',
      body: {
        priceList: cartBody.deliveries[0].priceList,
        storeId: cartBody.deliveries[0].storeId,
        ffmcenter: cartBody.deliveries[0].ffmcenter,
        ffmCenterOrder: submitOrderResponse
      }
    };    

    try {
      const cartBody = await cartRequest( cartOptions );
      return handleSuccess( req, res, cartBody );
    } catch ( setStatusError ) {
      return handleError( req, res, setStatusError );
    }

  };

};

const _invoice = async ( req, res ) => {
  const invoiceBody = req.body;

  const invoiceOptions = {
    body: {
      header: { 
        function: "postBillingInvoice", 
        brand: process.env.MQ_BRAND_BILLING, 
        login: res.locals.login, 
        version: 'v2',
        systemSource: "APP-VENDEDOR",
        id: invoiceBody.shoppingCartId
      },
      body: invoiceBody
    },
    method: "POST",
    timeout: process.env.GAN_TIMEOUT || 5000
  };

  try {
    const invoiceResponse = await ganRequest( invoiceOptions );
    return handleSuccess( req, res, invoiceResponse );
  } catch ( invoiceError ) {
    return handleError( req, res, invoiceError );
  };

};

const _setStatus = async ( req, res ) => {
  const newStatus = req.params.newStatus;
  const cartId = req.params.cartId;

  // Se for cancelamento, irá remover a reserva/apagar o pedido
  if ( newStatus === 'X' ) {
    const cartOptions = {
      path: `/get/${cartId}`,
      method: 'GET'
    };

    let cartBody; // Objeto com o conteúdo do carrinho
    try {
      cartBody = await cartRequest( cartOptions );
    } catch ( cartError ) {
      return handleError( req, res, cartError );
    }

    // Itens para remover a reserva
    const itemsToDelete = [];
    cartBody.deliveries.forEach( delivery => delivery.orderItems.forEach( item => itemsToDelete.push( { sku: item.sku, quantity: item.quantity } ) ) );

    if ( itemsToDelete.length > 0 ) {
      const ganOptions = {
        body: {
          header: { function: "deleteReservation", login: res.locals.login },
          body: {
            storeId: cartBody.deliveries[0].storeId || null,
            priceList: cartBody.deliveries[0].priceList || null,
            orderId: cartBody.id,
            salesMan: cartBody.salesMan,
            items: itemsToDelete,
            cartId: cartBody.id
          }
        },
        method: "POST"
      };
  
      // Se possuir um pedido, não deve realizar a deleção da reserva e sim a deleção do pedido.
      if ( cartBody.deliveries[0].orderId ) {
        ganOptions.body.header.function = 'deleteOrder';
        ganOptions.body.header.brand = 'APISERVICEORDER';
        ganOptions.body.body.orderId = parseInt( cartBody.deliveries[0].orderId );
      };
  
      try {
        ganRequest( ganOptions );
      } catch ( ganError ) {
        console.log( `Erro ao remover pedido/reserva. Carrinho: ${cartId}` );
      };  
    }
  }

  const path = !req.query.errors ? `/setStatus/${cartId}/${newStatus}/${req.query.state}` : 
    `/setStatus/${cartId}/${newStatus}/${req.query.state}?errors=${req.query.errors || null}`;

  const cartOptions = {
    path: path,
    method: 'POST',
    body: req.body || null,
    version: 'v2'
  };

  try {
    const cartResponse = await cartRequest( cartOptions );
    return handleSuccess( req, res, cartResponse );
  } catch ( cartError ) {
    return handleError( req, res, cartError );
  };

};

const _error = async ( req, res ) => {
  const setError = {
    path: `/setError/${req.params.cartId}`,
    method: 'POST',
    version: 'v2',
    body: req.body
  };

  try {
    const cartResponse = await cartRequest( setError );
    return handleSuccess( req, res, cartResponse );
  } catch ( cartError ) {
    return handleError( req, res, cartError );
  };
};

module.exports = {
  checkout: _checkout

, error: _error
, invoice: _invoice
, setStatus: _setStatus

, get: _get
, getBySeller: _getBySeller
, getBySellerStatus: _getBySellerStatus
, getBySellerStatusState: _getBySellerStatusState
, getByCustomer: _getByCustomer
};