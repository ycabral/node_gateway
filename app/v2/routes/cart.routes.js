/**
 * @module Cart Routes Router Module
 * @file Arquivo que importa todas as rotas. Este arquivo é importado em ../config/express.js
 * @author Yuri Cabral - ycabral@br.ibm.com
 * @date 13/05/19
 */

const router = require('express').Router(); // Objeto contendo todas as rotas do carrinho
const cartController = require('../controllers/cart.controller')
  , { billingValidator, orderValidator_v2 } = require('../../services/helpers/validators.handler');

// Cart GETTERS
  router
    .get( '/cart/:cartId', cartController.get )
    .get( '/cart/seller/:sellerId', cartController.getBySeller )
    .get( '/cart/seller/:sellerId/:status', cartController.getBySellerStatus )
    .get( '/cart/seller/:sellerId/:status/:state', cartController.getBySellerStatusState )
    .get( '/cart/customer/:document', cartController.getByCustomer )
    .post( '/cart/checkout/:cartId', orderValidator_v2, cartController.checkout )
    .post( '/cart/status/error/:cartId', cartController.error )
    .post( '/cart/status/:cartId/:newStatus', cartController.setStatus )
    .post( '/cart/billing/invoice', cartController.invoice );

    // Comentário para teste do commit

module.exports = router;