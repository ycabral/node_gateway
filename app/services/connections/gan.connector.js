const requestPromise = require('promise-request-retry');

const _ganRequest = async ( options ) => {
  
  if ( !options.path ) {
    options.path = '/generic';
  };

  options.headers = { 
    ...options.headers, 
    'X-App-Key': process.env.APIGEE_APP_KEY,
    'X-App-Token': process.env.APIGEE_APP_TOKEN
  };

  options.timeout = options.timeout || 5000; // Timeout em ms
  options.url = process.env.GAN_ADDRESS + options.path;
  options.json = true;
  options.logger = console;
  options.retry = process.env.RETRY || 2;
  options.uri = `${options.url}/${options.body.header.function}`;

  options.body.header.debug = process.env.NODE_ENV !== 'production' ? true : false;

  return new Promise( (resolve, reject) => {
    requestPromise( options )
      .then( parsedBody => parsedBody.error ? reject( { statusCode: parsedBody.code, ...parsedBody} ) : resolve( parsedBody ) )
      .catch( err => reject( err.error ) );
  });
};

module.exports = {
  ganRequest: _ganRequest
};