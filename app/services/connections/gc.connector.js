const requestPromise = require('promise-request-retry');

const _gcRequest = async options => {
  
  if ( !options.path ) {
    throw ( new Error( { message: 'Path not informed' } ) );
  };

  options.headers = {
    Authorization: process.env.GC_TOKEN
  };

  options.timeout = options.timeout || 5000; // Timeout em ms
  options.url = process.env.GC_ADDRESS + options.path;
  options.json = true;
  options.logger = console;
  options.retry = process.env.RETRY || 2;
  options.uri = options.url;

  return new Promise( (resolve, reject) => {
    requestPromise( options )
      .then( parsedBody => {
        
        if ( /nao encontrada/g.test( parsedBody ) ) {
          return reject( { "message": "Conta não encontrada", statusCode: 404 } );
        };

        resolve( parsedBody );
      })
      .catch( err => {
        err.error.statusCode = err.statusCode;
        reject( err.error );
      });
  });
};

module.exports = {
  gcRequest: _gcRequest
};