const { ganRequest } = require('./gan.connector')
    , { cartRequest } = require('./cart.connector')
    , { weddingRequest } = require('./wedding.connector')
    , { esbRequest } = require('./esb.connector')
    , { shippingRequest } = require('./shipping.connector')
    , { ajaxPriceRequest } = require('./ajaxprice.connector')
    , { adminRequest } = require('./admin.connector')
    , { gcRequest } = require('./gc.connector')
    , { wcsRequest } = require('./wcs.connector');

module.exports = {
  ganRequest: ganRequest
, cartRequest: cartRequest
, weddingRequest: weddingRequest
, esbRequest: esbRequest
, shippingRequest: shippingRequest
, ajaxPriceRequest: ajaxPriceRequest
, wcsRequest: wcsRequest
, adminRequest: adminRequest
, gcRequest: gcRequest
};