const requestPromise = require('promise-request-retry');

const _adminRequest = async options => {
  // Define qual a rota para envio através do campo options.from 
  if ( options.from ) {
    options.from = options.from.toUpperCase();

    if ( options.from === 'EMAIL'   ) options.url = process.env.ADMIN_EMAIL_ADDRESS;
    if ( options.from === 'WEDDING' ) options.url = process.env.ADMIN_PRODUCTS_ADDRESS;
  };

  if ( options.from === 'WEDDING' ) {
    const adminOptions = {
      "header": {
        "method": "POST",
        "source": "ESB",
        "id": options.weddingListNumber,
        "path": `/v2/catalog/product/bySku/WeddingAPI.svc/products/${options.weddingListNumber}`
      },
    };

    options.body = { ...options, ...adminOptions };
  };

  options.timeout = options.timeout || 5000; // Timeout em ms 
  options.json = true;
  options.logger = console;
  options.retry = process.env.RETRY || 2;
  options.uri = options.url;

  return new Promise( (resolve, reject) => {
    requestPromise( options )
      .then( parsedBody => {

        if ( options.type === 'EMAIL' ) {
          return resolve( parsedBody );
        }

        if ( options.type === 'WEDDING' ) {
          if ( parsedBody.products instanceof Array && parsedBody.products.length > 0 ) {
            return resolve( parsedBody );
          };
  
          reject( { message: "Lista não possui produtos ou não existe", statusCode: 404 } );
        } 

      })
      .catch( err => {
        err.error.statusCode = err.statusCode;
        reject( err.error );
      });

  });
};

module.exports = {
  adminRequest: _adminRequest
};