const requestPromise = require('promise-request-retry');

const _wcsRequest = async options => {
  
  if ( !options.path ) {
    throw ( new Error( { message: 'Path not informed' } ) );
  };

  options.timeout = options.timeout || 5000; // Timeout em ms
  options.url = process.env.WCS_ADDRESS + options.path;
  options.json = true;
  options.logger = console;
  options.headers = { Host: "www.fastshop.com.br" };
  options.retry = process.env.RETRY || 2;
  options.uri = options.url;

  return new Promise( (resolve, reject) => {
    requestPromise( options )
      .then( parsedBody => resolve( parsedBody ) )
      .catch( err => {
        err.error.statusCode = err.statusCode;
        reject( err.error );
      });

  });
};

module.exports = {
  wcsRequest: _wcsRequest
};