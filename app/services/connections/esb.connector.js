const requestPromise = require('promise-request-retry');

const _esbRequest = async options => {

  if ( !options.path ) {
    throw ( new Error( { message: 'Path not informed' } ) );
  };

  // Define qual a rota para envio através do campo options.from 
  if ( options.from ) {
    options.from = options.from.toUpperCase();

    if ( options.from === 'SHIPPINGVTEX'  ) options.url = process.env.VTEX_ADDRESS            + options.path;
    if ( options.from === 'BILLING'       ) options.url = process.env.ADMIN_ADDRESS           + options.path;
    if ( options.from === 'CUSTOMERSYNC'  ) options.url = process.env.CUSTOMER_SYNC_ADDRESS   + options.path;
    if ( options.from === 'SUBMITORDER'   ) options.url = process.env.ESB_ADDRESS             + options.path;
  };

  options.timeout = options.timeout || 260000;
  options.json = true;
  options.logger = console;
  options.retry = process.env.RETRY || 2;
  options.uri = options.url;

  return new Promise( (resolve, reject) => {
    requestPromise( options )
      .then( parsedBody => {
        // Para cada tipo de options.from, é retornado uma estrutura diferente. Neste ponto, é feita a normalização.
        
        // Cálculo de frete de produtos marketplace
        if ( options.from === 'SHIPPINGVTEX' ) {
          if ( parsedBody.Status && parseInt( parsedBody.Status.Code ) !== 0 ) {
            const error = {
              ...parsedBody.Status,
              error: parsedBody.Status,
              message: parsedBody.Status.Detail,
              statusCode: 404
            };

            return reject( error );
          };

          if ( parsedBody.consultShippingRuleResponse.Status.Code && parseInt( parsedBody.consultShippingRuleResponse.Status.Code ) !== 0 ) {
            const error = {
              ...parsedBody.consultShippingRuleResponse.Status,
              error: parsedBody.consultShippingRuleResponse.Status,
              message: parsedBody.consultShippingRuleResponse.Status.Detail,
              statusCode: 404
            };

            return reject( error );
          }

          return resolve( parsedBody.consultShippingRuleResponse.Data);
        };

        if ( options.from === 'SUBMITORDER' ) {
          return resolve( parsedBody.error );
        };

        if ( options.from === 'BILLING' ) {
          return resolve( parsedBody );
        };
        
      })
      .catch( err => {
        // Para cada tipo de options.from, é retornado uma estrutura diferente. Neste ponto, é feita a normalização.
        
        if ( options.from === 'SUBMITORDER' && new RegExp(/successfully/g).test(err.error.error.detail) ) {
          return resolve( err.error.error.orderid );
        }

        reject( err.error );
      });
  });
};

module.exports = {
  esbRequest: _esbRequest
};