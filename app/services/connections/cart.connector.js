const requestPromise = require('promise-request-retry');

const _cartRequest = async options => {
  
  if ( !options.path ) {
    throw ( new Error( { message: 'Path not informed' } ) );
  };

  if ( !options.version ) {
    options.path = `/v1/shoppingcart${options.path}`;
  } else {
    options.path = `/${options.version}/shoppingcart${options.path}`;
  };

  options.timeout = options.timeout || 10000; // Timeout em ms
  options.url = process.env.CART_ADDRESS + options.path;
  options.json = true;
  options.logger = console;
  options.retry = process.env.RETRY || 2;
  options.uri = options.url;

  return new Promise( (resolve, reject) => {
    requestPromise( options )
      .then( parsedBody => {
        resolve( parsedBody );
      })
      .catch( err => {
        err.error.statusCode = err.statusCode;
        reject( err.error );
      });

  });
};

module.exports = {
  cartRequest: _cartRequest
};