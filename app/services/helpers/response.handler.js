const Prometheus = require('prom-client');
const httpRequestDurationMicroseconds = new Prometheus.Histogram({
  name: 'http_request_duration_ms',
  help: 'Duration of HTTP requests in ms',
  labelNames: ['method', 'route', 'code'],
  // buckets for response time from 0.1ms to 500ms
  buckets: [0.10, 5, 15, 50, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000]
});

const _handleError = ( req, res, err ) => {
  const responseTimeInMs = Date.now() - res.locals.startEpoch;

  httpRequestDurationMicroseconds
    .labels( req.method, req.route ? req.route.path ? req.route.path : '/apigw/seller/*' : '/apigw/seller/*', res.statusCode )
    .observe(responseTimeInMs);

  return res.status( err.statusCode || 500 ).send( err );
};

const _handleSuccess = ( req, res, body ) => {
  const responseTimeInMs = Date.now() - res.locals.startEpoch;

  httpRequestDurationMicroseconds
    .labels( req.method, req.route ? req.route.path ? req.route.path : '/apigw/seller/*' : '/apigw/seller/*', res.statusCode )
    .observe(responseTimeInMs);

  return res.status( body.statusCode || 200 ).send( body );
};

module.exports = {
    handleError: _handleError
  , handleSuccess: _handleSuccess
};