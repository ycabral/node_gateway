const { CART_CANCELLED, CART_CLOSED, CART_BLOCKED, GENERATING_INVOICE, GENERATING_ORDER } = require('./cart.handler')
  , { handleError } = require( '../../services/helpers/response.handler' )
  , { ganRequest, cartRequest } = require('../connections/connectors');

const _validateOrderCustomer = (customer, cartBody) => {

  let marketPlace = false;

  cartBody.deliveries.some( order => {
    marketPlace = order.isMarketplace;
    return marketPlace;
  });

  const errors = marketPlace ? [
      ..._validateOrderCustomerPhone( customer ), 
      ..._validateOrderCustomerAddress( customer, cartBody )
    ]
  :
    [
      ..._validateOrderCustomerAddress( customer, cartBody )
    ];
  
  if ( errors.length > 0 ) {
    throw ( { 
      errors: errors,
      message: errors.reduce( (message, err) => message + `\n${err}` ),
      statusCode: 400 
    } );
  };
  
  return true;
};

const _validateOrderCustomerPhone = customer => {

  const errors = [];

  customer.cellPhone = customer.cellPhone.replace(/\D/g, '');
  
  customer.cellPhone.length === 11 ? null : errors.push('Celular inválido para pedido marketplace');

  return errors;
};

const _validateOrderCustomerAddress = (customer, cartBody) => {
  
  const errors = [];

  let cartValidation;
  let sequence;
  if ( cartBody ) {
    cartValidation = cartBody.deliveryAddress ? cartBody.deliveryAddress.sequence ? sequence = cartBody.deliveryAddress.sequence : false : false;
  };

  customer.billingAddress.neighborhood.length <= 0 ? errors.push('Bairro de cobrança está em branco') : null;
  customer.billingAddress.number.length       <= 0 ? errors.push('Numero de cobrança está em branco') : null;
  customer.billingAddress.streetName.length   <= 0 ? errors.push('Logradouro de cobrança está em branco') : null;
  customer.billingAddress.streetType.length   <= 0 ? errors.push('Tipo de logradouro de cobrança está em branco') : null;
  customer.billingAddress.type.length         <= 0 ? errors.push('Tipo de moradia de cobrança está em branco') : null;
  customer.billingAddress.zipCode.length      <= 0 ? errors.push('CEP de cobrança está em branco') : null;

  if ( cartValidation && sequence ) {
    customer.deliveryAddress.some( address => {
      if ( address.sequence === sequence ) {
        address.neighborhood.length <= 0 ? errors.push('Bairro de entrega está em branco') : null;
        address.number.length       <= 0 ? errors.push('Numero de entrega está em branco') : null;
        address.streetName.length   <= 0 ? errors.push('Logradouro de entrega está em branco') : null;
        address.streetType.length   <= 0 ? errors.push('Tipo de logradouro de entrega está em branco') : null;
        address.type.length         <= 0 ? errors.push('Tipo de moradia de entrega está em branco') : null;
        address.zipCode.length      <= 0 ? errors.push('CEP de entrega está em branco') : null;
        return true;
      };
    });
  };
  
  return errors;
};

const _cartValidator = (cartBody, type) => {
  type = type || null;

  // Valida se o carrinho já foi finalizado
  if ( cartBody.deliveries.length > 0 && ( !cartBody.deliveries[0].isRemotePayment && cartBody.deliveries[0].orderId !== null ) ) throw ( { message: 'Cart already finished', statusCode: 400 } );
  
  // Valida se o carrinho já foi enviado ao GAN
  if ( cartBody.status.value === CART_CLOSED ) throw ( { message: 'Cart already sent to GAN', statusCode: 400 } );

  // Valida se o carrinho foi cancelado
  if ( cartBody.status.value === CART_CANCELLED ) throw ( { message: 'Cart had been already cancelled', statusCode: 400 } );

  // Tipo 0 significa que não é na geração do pedido (postOrder)
  if ( type !== 1 ) {
    // Valida se o carrinho está bloqueado
    if ( cartBody.status.value === CART_BLOCKED ) throw ( { message: 'Cart is blocked', statusCode: 400 } );
  };

};

const _cartValidator_v2 = (cartBody, type) => {
  type = type || null;

  // Valida se o carrinho já foi finalizado
  if ( cartBody.deliveries.length > 0 && ( !cartBody.deliveries[0].isRemotePayment && cartBody.deliveries[0].orderId !== null ) ) {
    throw ( { message: 'Carrinho já foi finalizado', statusCode: 400 } );
  };
  
  // Valida se o carrinho já foi enviado ao GAN
  if ( type !== 'billing' && cartBody.status.value === CART_CLOSED )  throw ( { message: 'Carrinho já foi enviado ao GAN.', statusCode: 400 } );

  // Valida se o carrinho foi cancelado
  if ( cartBody.status.value === CART_CANCELLED )                     throw ( { message: 'Carrinho foi cancelado.', statusCode: 400 } );

  // Valida se o carrinho está bloqueado
  if ( type !== 'order' && cartBody.status.value === CART_BLOCKED )   throw ( { message: 'Carrinho está bloqueado', statusCode: 400 } );

  // Descomentar esta API somente se o GAN engargalar durante a Golden Friday
  //if ( cartBody.status.value === GENERATING_ORDER )                   throw ( { message: 'Pedido está sendo gerado, aguarde.', statusCode: 400 } );

  //if ( cartBody.status.value === GENERATING_INVOICE )                 throw ( { message: 'Nota fiscal está sendo gerada, aguarde.', statusCode: 400 } );

};

const _orderValidator = async ( req, res, next ) => {
  const cartId = req.params.cartId || req.body.shoppingCartId;
  
  // Obtem o carrinho
  const cartOptions = {
    path: `/get/${cartId}`,
    method: 'GET'
  };
  
  let cartBody;
  try {
    cartBody = await cartRequest( cartOptions );
  } catch ( cartError ) {
    return handleError( req, res, cartError );
  }

  // Valida o status do carrinho
  try {
    _cartValidator( cartBody, 'order' );
  } catch (validationError) {
    return handleError( req, res, validationError );
  }

  // Obtém os dados do cliente no GAN
  const customerOptions = {
    body: {
      header: {
        function: "getCustomer"
      },
      body: {
        id: cartBody.customer.id
      }
    },
    method: "POST",
    path: '/generic'
  };

  let customer;
  try { 
    customer = await ganRequest( customerOptions );
  } catch ( getCustomerError ) {
    return handleError( req, res, getCustomerError );
  };

  try {
    _validateOrderCustomer( customer, cartBody );
  } catch ( customerError ) {
    return handleError( req, res, customerError );
  };

  // Atribui o carrinho para evitar requests de GET do carrinho nos próximos pontos
  res.locals.cartBody = cartBody;
  next();
};

const _orderValidator_v2 = async ( req, res, next ) => {
  const cartId = req.params.cartId || req.body.shoppingCartId;
  
  // Obtem o carrinho
  const cartOptions = {
    path: `/get/${cartId}`,
    method: 'GET',
    version: 'v2'
  };
  
  let cartBody;
  try {
    cartBody = await cartRequest( cartOptions );
  } catch ( cartError ) {
    return handleError( req, res, cartError );
  }

  // Valida o status do carrinho
  try {
    _cartValidator_v2( cartBody );
  } catch (validationError) {
    return handleError( req, res, validationError );
  }

  // Obtém os dados do cliente no GAN
  const customerOptions = {
    body: {
      header: {
        function: "getCustomer"
      },
      body: {
        id: cartBody.customer.id
      }
    },
    method: "POST",
    path: '/generic'
  };

  let customer;
  try { 
    customer = await ganRequest( customerOptions );
  } catch ( getCustomerError ) {
    return handleError( req, res, getCustomerError );
  };

  try {
    _validateOrderCustomer( customer, cartBody );
  } catch ( customerError ) {
    return handleError( req, res, customerError );
  };

  // Atribui o carrinho para evitar requests de GET do carrinho nos próximos pontos
  res.locals.cartBody = cartBody;
  next();
};

const _billingValidator = async ( req, res, next ) => {
  const billingBody = req.body;
  const validationErrors = [];

  if ( !billingBody.shoppingCartId || !/[0-9a-fA-F]{24}/g.test( billingBody.shoppingCartId ) ) {
    validationErrors.push( 'shoppingCartId inválido' );
  }

  if ( !billingBody.customerId ) validationErrors.push( 'Cliente não enviado' );
  if ( !billingBody.cashierStoreId ) validationErrors.push( 'Filial do caixa não enviada' );
  if ( !billingBody.cashierNumber ) validationErrors.push( 'Numero do caixa não enviado' );
  if ( !billingBody.login ) validationErrors.push( 'Login do vendedor não enviado' );
  if ( !billingBody.systemSource ) validationErrors.push( 'Sistema de origem não enviado' );
  if ( !billingBody.orders && billingBody.orders.length === 0 ) {
    validationErrors.push( 'Pedidos a serem faturados não foram enviados' ); 
  } else {
    billingBody.orders.forEach( order => {
      if ( !order.priceList ) validationErrors.push( 'Tabela do pedido não foi enviada' );
      if ( !order.storeId ) validationErrors.push( 'Filial do pedido não foi enviada' );
      if ( !order.orderId ) validationErrors.push( 'Numero do pedido não foi enviado' );
    }); 
  };
  
  if ( validationErrors.length > 0 ) {
    let responseMessage = '';

    validationErrors.forEach( (error, index) => {
      index+1 !== validationErrors.length ? responseMessage += error + '\n' : responseMessage += error ; 
    });

    const responseBody = { message: responseMessage, errors: validationErrors, statusCode: 400 };

    return handleError( req, res, responseBody );
  };

  next();

};

module.exports = {
  validateOrderCustomer: _validateOrderCustomer

, cartValidator: _cartValidator
, cartValidator_v2: _cartValidator_v2

, orderValidator: _orderValidator
, orderValidator_v2: _orderValidator_v2

, billingValidator: _billingValidator
};