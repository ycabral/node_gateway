
// Encapsulamento do método Promise.all para retornar todas as promises mesmo com erro em alguma delas.
const _handleMultipleRequest = async (promises, onlyFulfilled) => {
  const results = await Promise.all(promises.map(p => p.catch(e => e)));
  const validResults = results.filter(result => !(result instanceof Error));
  
  if ( onlyFulfilled ) {
    return validResults;
  }

  return results;
};


module.exports = {
  handleMultipleRequest: _handleMultipleRequest
};