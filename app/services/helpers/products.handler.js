const REGEX_PRODUCT = new RegExp( /_PRD/g ); // Valida se possui _PRD no produto
//const REGEX_WCS     = new RegExp( /[^\/]*$/g ); // Valida se possui /* na string 

const _obtemItensMarketplace = (object, type) => {

  const marketplaceProducts = [];

  // Obtém itens marketplace para cálculo de entrega
  if ( type === 0 ) {
    object.forEach( item => item.marketPlace ? marketplaceProducts.push({
        SKU: item.sku,
        Quantity: parseInt(item.quantity)
      }) : false
    );
  };
  
  // Obtem itens marketplace a partir do carrinho
  if ( type === 1 ) {
    object.forEach( item => item.isMarketplace ? marketplaceProducts.push({
        sku: item.sku,
        quantity: parseInt(item.quantity)
      }) : false
    );
  };

  // Obtem itens para enviar ao AJAX PRICE
  if ( type === 2 ) {

    // Se obj de entrada for array, retornará todos os itens dentro do array.
    if ( object instanceof Array ) {
      object.forEach( product => {
        if ( product.details.MarketPlaceStore || product.details.MarketPlaceStore !== undefined ) {
          product.skus.forEach( productVoltage => {
            marketplaceProducts.push( productVoltage.sku );
          });
        };
      });
    }
  }

  return marketplaceProducts;
  
};

const _obtemItensFastShop = (object, type) => {
  const fastShopProducts = [];

  // Retorna os produtos para a request de cálculo de frete (shipping)
  if ( type === 0 ) {
    object.products.forEach( product => {
      if ( !product.marketPlace && !/^G(2|3|X|S|Q|K)|^SX/g.test( product.sku )  ) {
        fastShopProducts.push({
          cdsDisponiveis: [object.storeId === '63' ? '33' : object.storeId], // Trata cenário de regime especial (converte 63 para 33)
          quantidade: product.quantity,
          tipo: 'produto',
          valorProduto: product.value,
          sku: product.sku
        });
      };
    });
  }

  // Retorna os produtos a serem reservados no GAN
  if ( type === 1 ) {

    // Se for um array, deve iterar no array (geralmente, é chamado quando iterando os itens do carrinho)
    if ( object instanceof Array ) {
      object.forEach( item => {
        if ( !item.isMarketplace ) {
          fastShopProducts.push({ sku: item.sku, quantity: parseInt(item.quantity), isWeddingGift: item.isWeddingGift, isRemotePayment: item.isRemotePayment });
        };
      });
    } else {
      fastShopProducts.push({ sku: object.sku, quantity: parseInt(object.quantity), isWeddingGift: object.isWeddingGift, isRemotePayment: object.isRemotePayment });
    }

  }

  // Retorna os produtos a terem o preço consultado no GAN
  if ( type === 2 ) {

    // Se obj de entrada for array, retornará todos os itens dentro do array.
    if ( object instanceof Array ) {
      object.forEach( product => {
        if ( !product.details.MarketPlaceStore || product.details.MarketPlaceStore === undefined ) {
          product.skus.forEach( sku => {
            fastShopProducts.push( sku.sku.replace(REGEX_PRODUCT, '') );
          });
        };
      });
    };

  };

  // Retorna os produtos com base na resposta do ADMIN (cenário lista de casamento)
  if ( type === 3 ) {

    // Se obj de entrada for array, retornará todos os itens dentro do array.
    if ( object instanceof Array ) {
      object.forEach( product => {
        fastShopProducts.push( product.idSku.replace(REGEX_PRODUCT, '') );
      });
    };

  };

  return fastShopProducts;
};

module.exports = {
  obtemItensMarketplace: _obtemItensMarketplace
, obtemItensFastShop: _obtemItensFastShop
};