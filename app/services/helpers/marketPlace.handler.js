const { ganRequest } = require('../connections/connectors');

const _deParaParcela = async parcela => {
  const getPaymentOptions = {
    body: {
      header: {
        function: "getPaymentConditions", brand: process.env.MQ_BRAND_PRICE
      },
      body: {}
    },
    method: "POST",
    path: '/generic'
  };
  
  let paymentResponse;

  return new Promise( async (resolve, reject) => {
    try {
      paymentResponse = await ganRequest( getPaymentOptions );
    } catch ( getPaymentError ) {
      reject( getPaymentError );
    };
        
    paymentResponse.some( paymentTerm => {
      if ( paymentTerm.id === parcela.toString() ) {
        return parcela = paymentTerm.quotes.toString();
      };
    });

    resolve( parcela );

  });
};

const _deParaCondicao = condicao => {
  
  let codigo = '';
  switch ( condicao ) {
    case '79': 
      codigo = '5004';
      break;
    case '61': 
      codigo = '5005';
      break;
    case '74': 
      codigo = '5010';
      break;
    case '67': 
      codigo = '5011';
      break;
    case '86': 
      codigo = '5012';
      break;
    case '96': 
      codigo = '5013';
      break;
    case '83': 
      codigo = '5014';
      break;
    case '71': 
      codigo = '5015';
      break;
    case '81': 
      codigo = '5016';
      break;
    case '75': 
      codigo = '5017';
      break;
    case '76': 
      codigo = '5018';
      break;
    case '77': 
      codigo = '5019';
      break;
  };

  return codigo;

};

const _right = (str, n) => {
    if (n <= 0)
       return "";
    else if (n > String(str).length)
       return str;
    else {
       let iLen = String(str).length;
       return String(str).substring(iLen, iLen - n);
    }
};

const _idGenerator = (type, id) => {

  if ( type === 1 ) {
    return `AP${id.substring(id.length-7, id.length)}`;
  }

  let text = "";
  let dd;
  let yy;
  let ss;
  let ww;
  
  let date = new Date();
  
  yy = date.getYear().toString(); // year
  ww = date.getDay().toString(); //weak
  dd = date.getDate().toString();
  ss = date.getMilliseconds().toString();

  let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

  for (let i = 0; i < 5; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));
    
    text = _right(text.toUpperCase(),1) + _right(yy,2) + dd + ww + ss + text.toUpperCase();

  return text.substring(0,9);
};

module.exports = {
  idGenerator: _idGenerator
, deParaParcela: _deParaParcela
, deParaCondicao: _deParaCondicao
};