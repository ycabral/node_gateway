const { obtemItensFastShop } = require('./products.handler');

const _shippingBuilder = ( type, req, cartObject ) => {

  const items = { storeId: req.body.storeId , products: [...cartObject.deliveries[0].orderItems] };
  const fastShopProducts = obtemItensFastShop( items, 0 );

  req.body.shippingData[0].prazoEmDiaUteis = req.body.shippingData[0].prazoEmDiasUteis;
  req.body.shippingData[0].prazoEmDiaNaoUteis = req.body.shippingData[0].prazoEmDiasNaoUteis;
  delete req.body.shippingData[0].prazoEmDiasUteis;
  delete req.body.shippingData[0].prazoEmDiasNaoUteis;

  req.body.shippingData[0].dataBase = `${req.body.shippingData[0].dataBase.substr(0,4)}-${req.body.shippingData[0].dataBase.substr(4,2)}-${req.body.shippingData[0].dataBase.substr(6,2)}T00:00:00`;
  req.body.shippingData[0].dataPrevistaEntrega = `${req.body.shippingData[0].dataPrevistaEntrega.substr(0,4)}-${req.body.shippingData[0].dataPrevistaEntrega.substr(4,2)}-${req.body.shippingData[0].dataPrevistaEntrega.substr(6,2)}T00:00:00`;
  req.body.shippingData[0].dataPrevistaColeta = `${req.body.shippingData[0].dataPrevistaColeta.substr(0,4)}-${req.body.shippingData[0].dataPrevistaColeta.substr(4,2)}-${req.body.shippingData[0].dataPrevistaColeta.substr(6,2)}T00:00:00`;

  
  if ( type === 'scheduled' ) {
    req.body.shippingData[0].agenda[0].dataBase = `${req.body.shippingData[0].agenda[0].dataBase.substr(0,4)}-${req.body.shippingData[0].agenda[0].dataBase.substr(4,2)}-${req.body.shippingData[0].agenda[0].dataBase.substr(6,2)}T00:00:00`;
    req.body.shippingData[0].agenda[0].dataPrevistaEntrega = `${req.body.shippingData[0].agenda[0].dataPrevistaEntrega.substr(0,4)}-${req.body.shippingData[0].agenda[0].dataPrevistaEntrega.substr(4,2)}-${req.body.shippingData[0].agenda[0].dataPrevistaEntrega.substr(6,2)}T00:00:00`;
    req.body.shippingData[0].agenda[0].dataPrevistaColeto = `${req.body.shippingData[0].agenda[0].dataPrevistaColeto.substr(0,4)}-${req.body.shippingData[0].agenda[0].dataPrevistaColeto.substr(4,2)}-${req.body.shippingData[0].agenda[0].dataPrevistaColeto.substr(6,2)}T00:00:00`;
  };

  const requestBody = {
    header: [{
      method: 'POST',
      path: '/api/efetivacao/reservas',
      id: req.body.zipCode.replace('-', ''),
      source: 'APPV'
    }],
    body: [{
      cep: req.body.zipCode.replace('-',''),
      dataBase: new Date( Date.now() ),
      aplicacao: 'appvendedor',
      pedido: cartObject.id,
      subCanal: null,
      tabVendas: cartObject.deliveries[0].priceList,
      tipoPagamento: null,
      entregas: [{
        cd: req.body.storeId === '63' ? '33' : req.body.storeId,
        listaProdutos: fastShopProducts,
        listaServicosLogisticos: [...req.body.shippingData]
      }]
    }]
  };

  return requestBody;
};

module.exports = {
  shippingBuilder: _shippingBuilder
};