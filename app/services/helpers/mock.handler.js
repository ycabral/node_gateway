const _getBundles = async ( options ) => new Promise( ( resolve, reject ) => {

  options.mock.resolve ? resolve(
    {
      skuList: [{
        sku: "JBLGO2PTOB",
        bundles: [
          {
            sku: "JBCJ123456",
            description: "CONJUNTO JBLGO2PTOB MOCKADO"
          },
          {
            sku: "JBCJ123457",
            description: "CONJUNTO 2 JBLGO2PTOB MOCKADO"
          },
        ]
      },{
        sku: "R6R690400PCNB",
        bundles: [
          {
            sku: "R6CJ10293810",
            description: "CONJUNTO R6R690400PCNB MOCKADO"
          },
          {
            sku: "R6CJDAOEIJDA",
            description: "CONJUNTO 2 R6R690400PCNB MOCKADO"
          },
        ]
      }]
    }
  ) : reject( { message: 'Erro' } );

});

const _getSku = async ( options ) => new Promise( ( resolve, reject ) => {
  options.mock.resolve ? resolve(
    {
      "buyable": false,
      "images": [],
      "isShippingAvailability": true,
      "longDescription": "CARACTERISTICA DO PRODUTO NO ECARACT",
      "manuals": [],
      "name": "ECADMAT->MDESC",
      "parentPartNumber": null,
      "productID": null,
      "shortDescription": "ECADMAT->MDESC",
      "thumbnail": null,
      "voltage": [
        {
          "catEntry": "4611686018427417390",
          "identifier": "Bivolt",
          "name": "Bivolt",
          "sku": "AECJMC240_DDR3",
        }
      ],
      "marketPlace": false,
      "marketPlaceFulfillmentCenter": null,
      "marketPlaceText": null,
      "remoteSale": false,
      "bundles": false
    }
  ) : reject( {message: "Deu M*" } );

});

module.exports = {
  getBundles: _getBundles
, getSku: _getSku
};