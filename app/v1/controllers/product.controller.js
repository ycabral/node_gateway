const { wcsRequest, ganRequest, ajaxPriceRequest } = require('../../services/connections/connectors');
const { handleError, handleSuccess } = require('../../services/helpers/response.handler');
const { obtemItensFastShop, obtemItensMarketplace } = require('../../services/helpers/products.handler');

const _getBySku = async ( req, res ) => {
  req.params.sku = req.params.sku.toUpperCase().replace(/_PRD/g, '');

  //if ( req.query.bundle !== 'true' ) {

    const wcsOptions = {
      method: 'GET',
      path: `/v1/seller/products/byPartNumber/${req.params.sku}`
    };
  
    // Envia a request para o WCS para obter os dados do produto
    let wcsResponse;
    try {
      wcsResponse = await wcsRequest( wcsOptions );
    } catch ( wcsError ) {
      return handleError( req, res, wcsError );
    };
  
    // Se for produto marketplace
    if ( wcsResponse.marketPlace ) {
      wcsResponse.voltage.forEach( voltage => {
        voltage.priceOffer = parseInt( wcsResponse.priceOffer.replace(/\D/g, '') );
        voltage.priceTag = parseInt( wcsResponse.priceTag.replace(/\D/g, '') );
        voltage.sku = wcsResponse.partNumber;
      });
      return handleSuccess( res, res, wcsResponse );
    }
  
    // Prepara a request para enviar ao GAN
    const priceOptions = {
      body: {
        header: { function: "getPrice", brand: process.env.MQ_BRAND_PRICE },
        body: {
          sku: [req.params.sku],
          priceList: req.query.priceList
        }
      },
      method: 'POST'
    };
  
    let priceResponse;
    try {
      priceResponse = await ganRequest( priceOptions );
    } catch ( priceError ) {
      return handleError( req, res, priceError );
    }
  
    // Ajusta o JSON para manter conforme especificação em swagger
    delete wcsResponse.priceOffer;
    delete wcsResponse.priceTag;
    delete wcsResponse.partNumber;
    wcsResponse.marketPlace = false;
    wcsResponse.marketPlaceText = wcsResponse.marketPlaceFulfillmentCenter = null;
  
    // Insere o preço do GAN no atributo "voltage" do JSON de retorno.
    wcsResponse.voltage.forEach( (wcsSku, index) => {
      priceResponse.skuList.forEach( ganSku => {
        if ( wcsSku.partNumber === ganSku.sku ) {
          delete wcsSku.partNumber;
          wcsResponse.voltage[index] = { ...wcsResponse.voltage[index], ...ganSku };
        }
      });
    });
  
    // Requisito para habilitar venda remota no APP do Vendedor
    wcsResponse.remoteSale = wcsResponse.buyable;
  
    // Realiza a busca no GAN dos conjuntos os quais o item pertence
    //const serviceOptions = {
    //  body: {
    //    header: { function: "getConjuntos" },
    //    body: {
    //      skuList: [req.params.sku],
    //      onlyServices: true
    //    }
    //  },
    //  method: "POST",
    //  mock: { resolve: true }
    //};
  
    //let ganBundles;
    //try {
    //  ganBundles = await ganRequest( serviceOptions );
    //} catch (serviceError) {
    //  wcsResponse.bundles = false;
    //  return handleSuccess( req, res, wcsResponse );
    //};
  
    //wcsResponse.voltage.forEach( product => {
    //  ganBundles.skuList.some( bundle => {
    //    bundle.sku === product.sku ? product.bundles = [...bundle.bundles] : false;
    //  });
    //});
  
    //wcsResponse.bundles = true;
  //
    return handleSuccess( req, res, wcsResponse );
    
  //};

  // Prepara a request para enviar ao GAN (obterá a estrutura do produto)
  //const skuOptions = {
  //  body: {
  //    header: { function: "getProduct" },
  //    body: {
  //      sku: req.params.sku
  //    }
  //  },
  //  method: 'POST',
  //  mock: { resolve: true }
  //};

  //let skuBody;
  //try { 
  //  skuBody = await ganRequest( skuOptions );
  //} catch ( skuError ) {
  //  return handleError( req, res, skuError );
  //};

  // Prepara a request para enviar ao GAN
  //const priceOptions = {
  //  body: {
  //    header: { function: "getPrice", brand: process.env.MQ_BRAND_PRICE },
  //    body: {
  //      sku: [req.params.sku],
  //      priceList: req.query.priceList
  //    }
  //  },
  //  method: 'POST'
  //};

  // Realiza a request de preço para o GAN
  //let priceResponse;
  //try {
  //  priceResponse = await ganRequest( priceOptions );
  //} catch ( err ) {
  //  return handleError( req, res, err );
  //}

  // Insere o preço do GAN no atributo "voltage" do JSON de retorno.
  //skuBody.voltage.forEach( (product, index) => {
  //  priceResponse.skuList.forEach( ganSku => {
  //    if ( product.sku === ganSku.sku ) {
  //      delete product.partNumber;
  //      skuBody.voltage[index] = { ...skuBody.voltage[index], ...ganSku };
  //      skuBody.buyable = ganSku.buyable;
  //    }
  //  });
  //});

  //return handleSuccess( req, res, skuBody );

};

const _getBySearchTerm = async ( req, res ) => {
  const reqProducts = req.body.products;
  const responseBody = { products: [] }; // Cria outro objeto para evitar manipulação com os dados originais
  const requests = []; // Array que conterá todas as requests a serem realizadas de forma paralela.

  const fastShopProducts = obtemItensFastShop( reqProducts, 2 );
  const marketPlaceProducts = obtemItensMarketplace( reqProducts, 2 );

  // Popula o array requests com todos os produtos marketplace
  if ( marketPlaceProducts.length > 0 ) {
    marketPlaceProducts.forEach( product => {
      requests.push( ajaxPriceRequest({
        method: 'GET',
        path: product
      }));
    });
  }

  // Popula o array requests com todos os produtos FastShop
  if ( fastShopProducts.length > 0 ) {
    // Prepara a request para enviar ao GAN
    const priceOptions = {
      body: {
        header: { function: "getPrice", brand: process.env.MQ_BRAND_PRICE },
        body: {
          sku: fastShopProducts,
          priceList: req.query.priceList
        }
      },
      method: 'POST'
    };

    requests.push( ganRequest( priceOptions ) );
  }

  // Realiza as requests de modo paralelo
  let productsPrices;
  let ganResponseIndex;
  
  try {
    productsPrices = await Promise.all( requests );

    // Obtem o indice de onde está posicionada a request do GAN (melhorar performance)
    productsPrices.some( (products,index) => {
      products.priceList ? ganResponseIndex = index : productsPrices[index].id = marketPlaceProducts[index];
    });
  } catch ( pricesErr ) {
    return handleError( req, res, pricesErr );
  };

  // Monta estrutura do JSON de resposta
  reqProducts.forEach( reqProduct => {
    const bodyToPush = {
      buyable: false,
      marketPlace: reqProduct.details.MarketPlaceStore ? reqProduct.details.MarketPlaceStore[0] ? true : false : false,
      marketPlaceText: reqProduct.details.MarketPlaceStore ? reqProduct.details.MarketPlaceStore[0] ? reqProduct.details.MarketPlaceStore[0] : '': '',
      marketPlaceFulfillmentCenter: reqProduct.details.MarketPlaceStore ? reqProduct.details.MarketPlaceStore[0] ? `MKT_${reqProduct.details.MarketPlaceStore[0].toUpperCase()}` : '' : '',
      shortDescription: reqProduct.name,
      name: reqProduct.name,
      thumbnail: reqProduct.images.default,
      voltage: [],
      details: reqProduct.details
    };

    // Obtem o catEntry do produto
    let catEntry = reqProduct.details.catalogEntryId ? 
      reqProduct.details.catalogEntryId[0] ? 
        reqProduct.details.catalogEntryId[0] : 'Não especificado'
      : 'Não especificado';

    reqProduct.skus.forEach( reqSku => {

      // Tratamento de produtos Marketplace
      if ( marketPlaceProducts.length > 0 ) {
        if ( reqProduct.details.MarketPlaceStore ) {
          return productsPrices.some( price => {
            if ( price.id === reqSku.sku ) {
              const maxQuotesAllowed = parseInt(price.installmentPrice.split(' ')[0]) ? parseInt(price.installmentPrice.split(' ')[0]) : 1;
              const installmentValue = parseInt(price.installmentPrice.split(' ')[3]) ? parseInt(price.installmentPrice.split(' ')[3].replace(/\D/g, '')) : parseInt( price.offerPriceValue ) * 100 ;
              const priceOffer = parseFloat(price.offerPrice.split(' ')[1]) ? parseFloat(price.offerPrice.split(' ')[1].replace(/\D/g, '')) : parseFloat(price.offerPrice); 
              const priceTag   = parseFloat(price.listPrice.split(' ')[1]) ? parseFloat(price.listPrice.split(' ')[1].replace(/\D/g, '')) : parseFloat(price.listPrice); 

              return priceOffer > 0 || priceTag > 0 ? bodyToPush.voltage.push({
                sku: reqSku.sku,
                voltage: reqSku.specs.Voltagem ? reqSku.specs.Voltagem[0] : "Não especificado",
                priceOffer: priceOffer || null,
                priceTag: priceTag || null,
                maxQuotesAllowed: maxQuotesAllowed,
                installmentValue: installmentValue,
                isM1: false,
                remoteSale: false,
                catEntry: price.catalogEntryId
              }) : false;
            };
          });
        };
      };

      // Tratamento de itens FastShop
      if ( fastShopProducts.length > 0 ) {
        productsPrices[ganResponseIndex].skuList.some( ganSku => {
          if ( ganSku.sku === reqSku.sku && ganSku.priceTag !== null  ) {
            if ( bodyToPush.buyable === false ) bodyToPush.buyable = ganSku.buyable;
            return bodyToPush.voltage.push({
              sku: reqSku.sku,
              voltage: reqSku.specs.Voltagem ? reqSku.specs.Voltagem[0] : "Não especificado",
              remoteSale: false,
              ...ganSku,
              catEntry: catEntry
            });
          };
        });
      };
    });

    responseBody.products.push( bodyToPush );
  });
  
  // Monta string de SKU's marketplace para enviar ao WCS (verificação de estoque)
  if (marketPlaceProducts.length > 0) {
    let stringSkuMPlace = '';
    responseBody.products.forEach( product => {
      product.voltage.forEach( voltageSku => {
        if (product.marketPlace) {
          stringSkuMPlace += voltageSku.catEntry + ',';
        }
      });
    });
    stringSkuMPlace = stringSkuMPlace.substr(0, stringSkuMPlace.length - 1); // removes colon at the end of the string to send to WCS
  
    const wcsOptions = {
      path: `/store/10151/inventoryavailability/${stringSkuMPlace}`,
      method: 'GET'
    };

    let wcsInventory;

    try {
      wcsInventory = await wcsRequest( wcsOptions );
    } catch ( wcsErr ) {
      responseBody.error += 'Inventory: WCS Request Error';
    }

    // Testa se a request deu sucesso
    if ( wcsInventory ) {
      wcsInventory.InventoryAvailability.forEach( wcsProduct => {
        responseBody.products.forEach( product => {
          product.voltage.some( voltageSku => {
            if ( wcsProduct.productId === voltageSku.catEntry ) {
              return product.buyable = product.buyable === false ? parseInt( wcsProduct.availableQuantity ) > 0 ? true : false : product.buyable;
            };
          });
        });
      });
    };
  };

  // Remove da resposta se o produto não possuir preço
  responseBody.products = responseBody.products.filter( product => !product.marketPlace ? product.voltage.length > 0 : true ) ;

  // Buscara os dados do produto no WCS para verificar se é um produto disponível para venda remota
  const wcsRequests = [];
  responseBody.products.forEach( product => 
    product.voltage.forEach( voltage => 
      wcsRequests.push( wcsRequest({
        path: `/v1/seller/products/byPartNumber/${voltage.sku}`,
        method: 'GET'
      }))
    )
  );

  // Realiza as requests ao WCS de forma paralela e obtém somente as respostas que deram sucesso de todas as requests realizadas.
  const wcsAllRequests = await Promise.all( wcsRequests.map( req => req.catch( err => err ) ) );
  const wcsValidResponses = wcsAllRequests.filter( result => !(result instanceof Error) );
  wcsValidResponses.forEach( wcsProduct => {
    responseBody.products.forEach( product => {
      if ( !product.marketPlace ) {
        product.voltage.forEach( voltage => {
          if ( voltage.sku === wcsProduct.partNumber ) {
            voltage.remoteSale = wcsProduct.buyable;
          };
        });
      };
    });
  });

  // Realiza a busca no GAN dos conjuntos os quais o item pertence
  //const serviceOptions = {
  //  body: {
  //    header: { function: "getConjuntos" },
  //    body: {
  //      skuList: fastShopProducts,
  //      onlyServices: true
  //    }
  //  },
  //  method: "POST",
  //  mock: { resolve: false }
  //};

  //let ganBundles;
  //try {
  //  ganBundles = await ganRequest( serviceOptions );
  //} catch (serviceError) {
  //  responseBody.products.forEach( product => product.bundle = false );
  //  return handleSuccess( req, res, responseBody );
  //};

  //responseBody.products.forEach( product => {
  //  if ( !product.marketPlace ) {
  //    product.voltage.forEach( sku => {
  //      product.bundle = false;
  //      ganBundles.skuList.some( bundle => {
  //        if ( bundle.sku === sku.sku ) {
  //          product.bundle = true;
  //          return sku.bundles = [...bundle.bundles];
  //        }
  //      });
  //    });  
  //  } else {
  //    product.bundle = false;
  //  };
  //});

  return handleSuccess( req, res, responseBody );

};

const _getAvailiabity = async ( req, res ) => {
  if ( req.query.isMPlace === 'true' ) {
    const wcsOptions = {
      path: `/store/10151/inventoryavailability/${req.params.sku}`,
      method: 'GET'
    };

    const responseBody = {
      available: [],
      sku: '',
      marketPlace: false,
      storeId: ''
    };

    let wcsInventory;
    try {
      wcsInventory = await wcsRequest( wcsOptions );
    } catch ( wcsError ) {
      return handleError( req, res, wcsError);
    }

    wcsInventory.InventoryAvailability.forEach( product => {
      responseBody.available.push( {  street: 'A', quantity: parseInt( product.availableQuantity ) } );
    });

    responseBody.sku = req.params.sku;
    responseBody.marketPlace = true;
    responseBody.storeId = "MarketPlace";

    return handleSuccess( req, res, responseBody );

  } else {
    let ganOptions;
    if ( req.query.storeId ) {
      ganOptions = {
        body: {
          header: { function: "getInventory", brand: process.env.MQ_BRAND_INVENTORY, login: res.locals.login },
          body: {
            sku: [req.params.sku],
            storeId: req.query.storeId
          }
        },
        method: "POST"
      };
    } else {
      ganOptions = {
        body: {
          header: { function: "getInventory", brand: process.env.MQ_BRAND_INVENTORY, login: res.locals.login },
          body: {
            sku: [req.params.sku]
          }
        },
        method: "POST"
      };
    };

    try {
      const ganResponse = await ganRequest( ganOptions );
      return handleSuccess( req, res, ganResponse );
    } catch ( ganError ) {
      return handleError( req, res, ganError );
    }
      
  };
};

module.exports = {
  getBySku: _getBySku
, getBySearchTerm: _getBySearchTerm
, getAvailiabity: _getAvailiabity
};