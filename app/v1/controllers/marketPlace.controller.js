/* eslint-disable max-statements */
const { esbRequest, ganRequest, wcsRequest } = require('../../services/connections/connectors')
    , { handleMultipleRequest } = require('../../services/helpers/promises.handler')
    , { deParaParcela, deParaCondicao,idGenerator } = require('../../services/helpers/marketPlace.handler');

const _customerSync = async cartObject => {
  const customerId = cartObject.customer.id;
  const sequence = cartObject.deliveryAddress.sequence;

  // Obtém os dados do cliente para montagem da request de customer sync
  const getCustomerOptions = {
    body: {
      header: {
        function: "getCustomer"
      },
      body: {
        id: customerId
      }
    },
    method: "POST",
    path: '/generic'
  };

  let customerBody;
  try {
    customerBody = await ganRequest( getCustomerOptions );
  } catch ( customerError ) {
    console.log( `Erro ao buscar cliente, customer sync. Carrinho: ${cartObject.id}
      Erro: ${customerError}` );
    return true; // Deve enviar o pedido mesmo sem o customer sync.
  }

  // Objeto com informações para o customer sync 
  const customerSyncOptions = {
    path: '/customersync/v1/submitCustomerSync',
    method: 'POST',
    from: "CUSTOMERSYNC",
    body: {
      submitCustomerSyncRequest: {
        CustomerData: {
          SourceID: "WCS",
          OperationType: "I",   // forçar update mesmo com novo cliente
          CustomerName: customerBody.name,
          CustomerType: customerBody.personType,
          Email: customerBody.email,
          Password: process.env.CUSTOMER_PASSWORD,
          DisableCustomer: "false"
        },
        _xmlns: "http://www.fastshop.com.br/TS/ECommCustomerSync/V2"
      }
    }
  };

  // se o telefone esta preenchido, enviar para o customerSync
  if (customerBody.phone.length > 0) {
    try {
      customerSyncOptions.body.submitCustomerSyncRequest.CustomerData.HomePhone = {
        AreaCode: getDdd(customerBody.phone),
        PhoneNumber: getPhone(customerBody.phone)
      };
    } catch (erro) {
      throw ( erro );
    };
  };

  // se o celular esta preenchido, enviar para o customerSync
  if (customerBody.cellPhone.length > 0) {
    try {
      customerSyncOptions.body.submitCustomerSyncRequest.CustomerData.MobilePhone = {
        AreaCode: getDdd(customerBody.cellPhone),
        PhoneNumber: getPhone(customerBody.cellPhone)
      };
    } catch (erro) {
      throw ( erro );
    };
  };

  if (customerBody.personType === 'J') {
    customerSyncOptions.body.submitCustomerSyncRequest.CustomerData.PJData = {
      CNPJ: customerBody.CNPJ,
      IE: customerBody.IE,
      CompanyName: customerBody.name
    };
  } else {
    customerSyncOptions.body.submitCustomerSyncRequest.CustomerData.PFData = {
      CPF: customerBody.CPF,
      RG: customerBody.RG,
      BirthDay: customerBody.dateBirth,
      Gender: customerBody.gender
    };
  };

  let customerAddress;
  
  if ( cartObject.deliveries[0].ffmcenter !== 'loja' ) {
    customerBody.deliveryAddress.some(address => address.sequence === sequence ? customerAddress = address : false);
  } else {
    customerAddress = customerBody.billingAddress;
  }

  customerSyncOptions.body.submitCustomerSyncRequest.CustomerData.MainAddress = {
    AddressName: 'Endereco de cadastro',
    StreetName: customerAddress.streetType + ' ' + customerAddress.streetName + ', ' + customerAddress.number,
    Number: customerAddress.number,
    Complement: customerAddress.complement || '',
    District: customerAddress.neighborhood,
    City: customerAddress.city,
    State: customerAddress.state,
    Country: 'Brasil',
    ZIPCode: customerAddress.zipCode.replace(/-/g, ''),
    StreetTypeId: customerAddress.streetType,
    HouseTypeId: '0'
  };

  // envia o CustomerSync para atualizar dados do cliente no ADMIN
  try {
      esbRequest(customerSyncOptions);
  } catch ( customerSyncError ) {
      console.log( `Erro ao realizar customer sync. Carrinho ${cartObject.id}` );
      /*throw( {message: `Nao foi possivel atualizar dados de cliente no ADMIN. Tenta novamente.`, 
              statusCode: 400});*/
  };

};

const _submitOrder = async (cartObject, type) => new Promise( async (resolve, reject) => {
  const customerId = cartObject.customer.id;
  //const sequence = cartObject.deliveryAddress.sequence;

  const getCustomer = {
    body: {
      header: {
        function: "getCustomer"
      },
      body: {
        id: customerId
      }
    },
    method: "POST",
    path: '/generic'
  };

  // obter dados do cliente pela API do GAN (getCustomer)
  let customerResponse;
  try {
    customerResponse = await ganRequest( getCustomer );
  } catch ( customerError ) {
    reject( { message: `Falha ao obter cliente. Carrinho: ${cartObject.id}`, error: customerError } );
  };
  
  const submitOrderOptions = {
    path: '/order/v8/submitorder',
    method: 'POST',
    from: "SUBMITORDER",
    body: {
      submitOrderRequest: {
        SubmitOrders: {
          Customer: {
            CustomerInfo: {
              ClientSystemID: "WCS",
              CustomerName: customerResponse.name,
              DocumentNumber: {},
              OtherDoc: "",
              DocumentType: "",
              Gender: customerResponse.gender,
              Email: customerResponse.email,
              SendEmail: "false",
              Birthday: customerResponse.dateBirth.substr( 0, 4 ) + '-' + 
                        customerResponse.dateBirth.substr( 4, 2 ) + '-' + 
                        customerResponse.dateBirth.substr( 6 )
            }
          }
        }
      }
    }
  };
  
  // Atribui os dados do cliente (pessoa física ou pessoa jurídica)
  if ( customerResponse.personType === 'F' ) {
    submitOrderOptions.body.submitOrderRequest.SubmitOrders.Customer.CustomerInfo.DocumentNumber.CPFNumber = customerResponse.CPF;
    submitOrderOptions.body.submitOrderRequest.SubmitOrders.Customer.CustomerInfo.OtherDoc = customerResponse.RG;
    submitOrderOptions.body.submitOrderRequest.SubmitOrders.Customer.CustomerInfo.DocumentType = 'RG';
  } else {
    submitOrderOptions.body.submitOrderRequest.SubmitOrders.Customer.CustomerInfo.DocumentNumber.CNPJNumber = customerResponse.CNPJ;
    submitOrderOptions.body.submitOrderRequest.SubmitOrders.Customer.CustomerInfo.OtherDoc = customerResponse.IE.replace(/\D/g, '');
    submitOrderOptions.body.submitOrderRequest.SubmitOrders.Customer.CustomerInfo.DocumentType = 'IE';  
  };
    
  // Atribui o endereço de entrega
  let zipCode;
  let addressName;

  const addressToSend = {};
  if ( cartObject.deliveries[0].ffmcenter !== 'loja' ) {
    customerResponse.deliveryAddress.some( address => {
      if ( address.sequence === cartObject.deliveryAddress.sequence ) {
        addressToSend.AddressName = parseFloat(address.type).toString() || "1" ;
        addressToSend.StreetName = address.streetName || "" ;
        addressToSend.Number = address.number || "" ;
        addressToSend.Complement = address.complement || "" ;
        addressToSend.StreetType = address.streetType || "" ;
        addressToSend.District = address.neighborhood || "" ;
        addressToSend.City = address.city || "" ;
        addressToSend.State = address.state || "" ;
        addressToSend.Country = 'BR' || "" ;
        //addressToSend.ZIPCode = address.zipCode.replace(/\D/g, '') || "" ;
        zipCode = address.zipCode.replace(/\D/g, '') || "" ;
      };
    });
  } else {
    addressToSend.AddressName = parseFloat(customerResponse.billingAddress.type).toString() || "1";
    addressToSend.StreetName = customerResponse.billingAddress.streetName || "" ;
    addressToSend.Number = customerResponse.billingAddress.number || "" ;
    addressToSend.Complement = customerResponse.billingAddress.complement || "" ;
    addressToSend.StreetType = customerResponse.billingAddress.streetType || "" ;
    addressToSend.District = customerResponse.billingAddress.neighborhood || "" ;
    addressToSend.City = customerResponse.billingAddress.city || "" ;
    addressToSend.State = customerResponse.billingAddress.state || "" ;
    addressToSend.Country = 'BR' || "" ;

    zipCode = customerResponse.billingAddress.zipCode.replace(/\D/g, '') || "" ;
  };
  

  // Atribui os dados de entrega do cliente
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Customer.CustomerInfo.Address = { ...addressToSend };
  // Foi utilizado desestrutuação de objeto neste ponto pois o objeto addressToSend é enviado em outros pontos.
  // desta forma, o ponteiro ao objeto "addressToSend" não é mantido, criando um novo objeto na memória em .CustomerInfo.Address
  addressToSend.ZIPCode = zipCode;
  if ( cartObject.deliveries[0].ffmcenter === 'loja' ) {
    addressToSend.AddressName = '6';
  }

  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Customer.CustomerInfo.HomePhone = {};
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Customer.CustomerInfo.HomePhone.PhoneNumber = customerResponse.cellPhone;
  
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.DeliveryAddress = {};
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.DeliveryAddress.Address = addressToSend;
  
  // Atribui os meios de contato
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.DeliveryPhone = {};
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.DeliveryPhone.PhoneNumber = customerResponse.cellPhone;

  // celular
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.DeliveryMobilePhone = {};
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.DeliveryMobilePhone.PhoneNumber = customerResponse.cellPhone;
  
  // Atribui outras informações
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.InventoryLocationID = cartObject.deliveries[0].expeditionStoreId ? cartObject.deliveries[0].expeditionStoreId: cartObject.deliveries[0].storeId;
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.SystemCalcDate = 'GAN';
  
  // Prepara os objetos com os dados do pedido
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order = {};
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.ClientOrder = {};
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.ClientOrder.ID = "0";
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.ClientOrder.DateTime = new Date();
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.Seller = {};
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.Shipping = {};
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.Shipping.ShippingAddress = {};
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.Shipping.ShippingAddress.Contact = {};
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.Shipping.ShippingAddress.Contact.Phone = {};
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.Shipping.ShippingAddress.Address = addressToSend;
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.OrderItems = {};
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.FulfillmentCenter = {};
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.OrderItems.OrderItem = [];
  if ( type === 1 ) {
    submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.StoreData = {};
    submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.StoreData.ReceiptName = 'Solicite documento e confira com a Nota Fiscal';
    submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.StoreData.OtherDoc = '1111111111111';
  };
  
  // Atribui os objetos com os dados do pedido
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.OrderDateTime = new Date();
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.Seller.ID = cartObject.salesMan;
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.CompanyID = '1';
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.CompanyBranchID = cartObject.deliveries[0].expeditionStoreId ? cartObject.deliveries[0].expeditionStoreId: cartObject.deliveries[0].storeId;
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.PricingTableID  = cartObject.deliveries[0].priceList;
  
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.SystemSource = 'APP';
  
  let subTotal = 0;
  let discountTotal = 0;
  let totalItems = 0;
  let skuToRequest = '';
  let fulfillmentCenter = '';
  const wcsRequests = [];
  
  cartObject.deliveries[0].orderItems.forEach( cartItem => {
    subTotal += (cartItem.unitPrice * cartItem.quantity) / 100;
    discountTotal += 0 ; 
    totalItems++;
    skuToRequest = cartItem.sku;
  
    wcsRequests.push( wcsRequest({
        method: 'GET',
        path: `/v1/seller/products/byPartNumber/${skuToRequest}`
      })
    );
  });
  
  let productResponse;
  try {
    productResponse = await handleMultipleRequest( wcsRequests );
  } catch ( productError ) {
    reject( { message: `Erro ao obter produtos do carrinho marketplace. Carrinho: ${cartObject.id}`, error: productError });
  };
  
  cartObject.deliveries[0].orderItems.forEach( cartItem => {
    productResponse.some( wcsObj => {
      if ( wcsObj.partNumber === cartItem.sku ) {
        submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.OrderItems.OrderItem.push( {
          SKU: cartItem.sku,
          itemName: wcsObj.name,
          itemCategory: cartItem.itemCategory.toLowerCase() === 'product' ? "PRODUTO" : "SERVICO",
          Quantity: cartItem.quantity.toString(),
          UnitPrice: ( cartItem.unitPrice / 100 ).toString(),
          TotalPrice: ( (cartItem.unitPrice * cartItem.quantity) / 100 ).toString(),
          ProductBundle: "0",
          FreightValue: ( cartItem.freightValue / 100 ).toString(),
          Interest: "0",
          Discount: ( 0 ).toString(),
          FulfillmentCenter: {
            Name: wcsObj.marketPlaceFulfillmentCenter
          }
        });
        fulfillmentCenter = wcsObj.marketPlaceFulfillmentCenter;
      }
    });
  });

  
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.Invoices = {
    InvoiceItems: [
      {
        AffiliateInvoiceVF: "",
        InvoiceVF: "",
        SerieInvoiceVF: "",
        SKUInvoice: "",
        SKUQuantityReturned: "0",
        ValueItemsReturned: "0",
        GiverListCode: ""
      }
    ]
  };
  
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.OrderSubtotalAmount = subTotal.toString();
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.InterestTotal = '0';
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.DiscountTotal = discountTotal.toString();
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.OrderTotalShip = ( cartObject.deliveries[0].shipping.deliveryCost / 100 ).toString();
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.OrderTotalAmount =
    ( parseFloat( submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.OrderSubtotalAmount) +
      parseFloat( submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.OrderTotalShip     ) +
      parseFloat( submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.InterestTotal      ) ).toFixed(2).toString();
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.OrderTotalItems = totalItems.toString();
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.IsWeddingList = "false";
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.CommitteeReturn = "0";
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.SessionID = cartObject.id;
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.QuantityMethodPayment = "1";
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.FulfillmentCenter.Name = fulfillmentCenter;
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.FulfillmentCenter.ID = "179000";

  // Pagamento Remoto
  if ( type === 1 ) {
    if ( cartObject.deliveries[0].ffmcenter === 'loja' ) {
      submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.VirtualPartnerID = '9';
    } else {
      submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.VirtualPartnerID = '2';
    };
  } else {
    submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.VirtualPartnerID = '0';
  };


  let date = new Date();
  let shippingDate = date.setDate( date.getDate() + 1 );
  shippingDate = new Date( shippingDate );

  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.Shipping.ShippingDate = shippingDate.toISOString().split('T')[0];

  const deliveryDate = cartObject.deliveries[0].shipping.deliveryDate;
  //const dateDelivery = new Date( parseInt( deliveryDate.substr( 0, 4 ) ),
  //                          parseInt( deliveryDate.substr( 4, 2 ) ) - 1,                       
  //                          parseInt( deliveryDate.substr( 6 ) )
  //);
  
  date = new Date();
  date = date.setHours( 0, 0, 0, 0 );
  //const timeDiff = Math.abs(dateDelivery.getTime() - date.getTime());
  //const estimatedDate = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
  
  if ( type === 1 ) {
    submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.Shipping.ShippingMethod = cartObject.deliveries[0].shipping.idReserva;

    if ( cartObject.deliveries[0].ffmcenter === 'loja' ) {
      submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.Shipping.ShippingAddress.Contact.ContactName = 'Solicite documento e confira com a Nota Fiscal';
      submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.Shipping.FreightValue = '0';
      submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.Shipping.DeliveryDate = '0001-01-03';
      submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.Shipping.ShippingDate = '0001-01-03';
      submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.Shipping.EstimatedShippingDays = 0;
    } else {
      submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.Shipping.ShippingAddress.Contact.ContactName = customerResponse.name;
      submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.Shipping.ShippingAddress.Contact.Phone = {};
      submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.Shipping.ShippingAddress.Contact.Phone.PhoneNumber = customerResponse.cellPhone;
      submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.Shipping.FreightValue = ( cartObject.deliveries[0].shipping.deliveryCost / 100 ).toString();
      submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.Shipping.ScheduledTimeSlot = cartObject.deliveries[0].shipping.shippingMethod;
      submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.Shipping.DeliveryDate = deliveryDate.substr( 0, 4 ) + '-' + 
                                                                                    deliveryDate.substr( 4, 2 ) + '-' + 
                                                                                    deliveryDate.substr( 6 );
      submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.Shipping.EstimatedShippingDays = cartObject.deliveries[0].shipping.daysToDelivery || 0;
      
      cartObject.deliveries[0].shipping.daysType && cartObject.deliveries[0].shipping.daysType != null ? 
        submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.Shipping.DaysType = cartObject.deliveries[0].shipping.daysType
      : null;
      
      submitOrderOptions.body.submitOrderRequest.SubmitOrders.DeliveryNameType = cartObject.deliveries[0].shipping.deliveryNameType;
    }
    
  } else { 
    submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.Shipping.ShippingMethod = cartObject.deliveries[0].shipping.shippingMethod.toLowerCase() === 'normal' ? '1': '2';
    submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.Shipping.ShippingAddress.Contact.ContactName = customerResponse.name;
    submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.Shipping.ShippingAddress.Contact.Phone = {};
    submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.Shipping.ShippingAddress.Contact.Phone.PhoneNumber = customerResponse.cellPhone;
    submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.Shipping.FreightValue = ( cartObject.deliveries[0].shipping.deliveryCost / 100 ).toString();
    submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.Shipping.ScheduledTimeSlot = cartObject.deliveries[0].shipping.shippingMethod;
    submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.Shipping.DeliveryDate = deliveryDate.substr( 0, 4 ) + '-' + 
                                                                                  deliveryDate.substr( 4, 2 ) + '-' + 
                                                                                  deliveryDate.substr( 6 );
    submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.Shipping.EstimatedShippingDays = cartObject.deliveries[0].shipping.daysToDelivery || 0;
    
    cartObject.deliveries[0].shipping.daysType && cartObject.deliveries[0].shipping.daysType != null ? 
      submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.Shipping.DaysType = cartObject.deliveries[0].shipping.daysType
    : null;
    
    submitOrderOptions.body.submitOrderRequest.SubmitOrders.DeliveryNameType = cartObject.deliveries[0].shipping.deliveryNameType;
    
  }
  
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.BillingData = {};
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.BillingData.BillingAddress = addressToSend;
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.BillingData.TicketMaturityTerm = '3';
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.BillingData.BillingsData = [];

  let paymentTerm;
  if ( type === 1 ) {
    cartObject.totalPayments.some( parcela => {
      paymentTerm = parcela.paymentTerm ? parcela.paymentTerm : null;
      return paymentTerm;
    });
  }
  
  const billingObject = {
    SequencePaymentID: "1",
    BillingMethodID: type === 1 ? deParaCondicao( cartObject.totalPayments[0].paymentTerm ? cartObject.totalPayments[0].paymentTerm : paymentTerm ) : "5010",
    BillingMethodDescription: "QRCode",
    BillingMethodPayValue: submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.OrderTotalAmount,
    FlagCardID: "56",
    CardHolderName: customerResponse.name,
    CardExpirationMonth: "08",
    CardExpirationYear: "2018",
    DocumentNumber: {},
    InstallmentValue: submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.OrderTotalAmount,
    InstallmentQuantity: "1"
  };
  
  customerResponse.personType === 'F' ?
    billingObject.DocumentNumber.CPF = customerResponse.CPF
  : billingObject.DocumentNumber.CNPJ = customerResponse.CNPJ;
  
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.BillingData.BillingsData.push( billingObject );
  
  const idPedido = cartObject.deliveries[0].orderId ? cartObject.deliveries[0].orderId : idGenerator();
  const billetDate = new Date().toISOString();
  
  let parcelResponse;

  try {
    parcelResponse = await deParaParcela( cartObject.totalPayments[0].paymentTerm ? cartObject.totalPayments[0].paymentTerm : paymentTerm );
  } catch ( parcelError ) {
    reject( { message: `Erro ao obter parcela. Carrinho: ${cartObject.id}`, error: parcelError });
  };
  
  // Envia um pagamento fake para cair no cenário de retentativa (gerar URL de pagamento para o cliente)
  const fakeBillingOptions = {
    path: '/OrderPayment/v2/insertSelectedPayments',
    method: 'POST',
    body: {
      InsertSelectedPayments: {
          selectedPayments: {
            SelectedPayment: [{
                BilletDate: billetDate,
                IdPaymentMethod: "5010",
                IdReceipt: "0",
                IdSelectedPayment: "0",
                Parcels: parcelResponse,
                Value: ( parseFloat( submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.OrderTotalAmount ) / parseFloat( parcelResponse ) ).toString(),
                VirtualPartnerIdReceipt: idPedido
            }]
          },
          accessKey: "FastShop" 
      }
    },
    from: 'BILLING' 
  };
  
  try {
    await esbRequest( fakeBillingOptions );
  } catch ( billingError ) {
    reject( { message: `Erro ao enviar pedido (1). Carrinho: ${cartObject.id}`, error: billingError });
  }
          
  // Envia o pedido para a VTEX
  submitOrderOptions.body.submitOrderRequest.SubmitOrders.Order.ClientOrder.ID = idPedido;
          
  try {
    const response = await esbRequest( submitOrderOptions );
    type === 1 ? resolve( response ) :  resolve( idPedido );
  } catch ( submitOrderError ) {
    reject( {message: `Erro ao enviar o pedido (2). Carrinho ${cartObject.id}`, error: submitOrderError } );
  };
});

const _validateCustomer = async ( cartObject ) => {
  const customerId = cartObject.customer.id;

  // Obtém os dados do cliente para montagem da request de customer sync
  const getCustomerOptions = {
    body: {
      header: {
        function: "getCustomer"
      },
      body: {
        id: customerId
      }
    },
    method: "POST",
    path: '/generic'
  };

  let customerBody;
  try {
    customerBody = await ganRequest( getCustomerOptions );
  } catch ( customerError ) {
    throw ( customerError );
  }

  if (customerBody.phone.length > 0) {
    try {
      getDdd(customerBody.cellPhone);
      getPhone(customerBody.cellPhone);
      getDdd(customerBody.phone);
      getPhone(customerBody.phone);
    } catch (erro) {
      throw ( erro );
    };
  };

  return true;

};

//
// funcao para extrair somente os digitos do DDD
// obrigatorio o formato (xx) xxxx-xxxx
//
// exemplo: entrada => (11) 3232-3000, retorno => 11
//
function getDdd( phone ) {
  const ddd = phone.substr(0,4); // extrai o DDD
  if (ddd.length !== 4 || ddd.replace(/\d/g,'') !== '()' || ddd.substr(0,1) !== '(' || ddd.substr(3,1) !== ')') {
    throw ( { message: "DDD inválido", statusCode: 400 } );
  }
  return ddd.replace(/\D/g,'');  // remove mascaras
};

//
// funcao para extrair somente os digitos do telefone
// obrigatorio o formato (xx) xxxx-xxxx
//
// exemplo: entrada => (11) 3232-3000, retorno => 32323000
//
function getPhone( phone ) {
  const ddd = getDdd(phone);
  let newPhone = phone.replace(`(${ddd})`,''); // remove o DDD
  newPhone = newPhone.replace(/\D/g,'');   // remove mascaras
  if (newPhone.length < 8 || newPhone.length > 9) {
    throw ( { message: "Telefone inválido", statusCode: 400 } );
  }
  return newPhone;
};

module.exports = {
  customerSync: _customerSync
, submitOrder: _submitOrder
, validateCustomer: _validateCustomer
};