const { handleError, handleSuccess } = require( '../../services/helpers/response.handler' )
    , { CART_CLOSED, CART_PENDING } = require('../../services/helpers/cart.handler')
    , { obtemItensMarketplace, obtemItensFastShop } = require('../../services/helpers/products.handler')
    , { ganRequest, cartRequest, esbRequest, shippingRequest, adminRequest } = require('../../services/connections/connectors')
    , { submitOrder, customerSync, validateCustomer } = require('./marketPlace.controller')
    , { cartValidator } = require('../../services/helpers/validators.handler')
    , { idGenerator } = require('../../services/helpers/marketPlace.handler')
    , { shippingBuilder } = require('../../services/helpers/shipping.handler')
    , moment = require('moment');

// Métodos de busca de carrinho
const _getCart = async ( req, res ) => {
  const getCart = {
    path: `/get/${req.params.cartId}`,
    method: 'GET' 
  };

  try {
    const cartResponse = await cartRequest( getCart );
    handleSuccess( req, res, cartResponse );
  } catch ( cartError ) {
    handleError( req, res, cartError );
  };
};

const _getCartsBySeller = async ( req, res ) => {
  const getCart = {
    path: `/findOrdersSeller/${req.params.sellerId}`,
    method: 'GET'
  };

  try {
    const cartResponse = await cartRequest( getCart );
    handleSuccess( req, res, cartResponse );
  } catch ( cartError ) {
    handleError( req, res, cartError );
  };
};

const _getCartsBySellerStatus = async ( req, res ) => {
  const getCart = {
    path: `/findBySellerIdAndStatus/${req.params.sellerId}/${req.params.status}/`,
    method: 'GET'
  };

  try {
    const cartResponse = await cartRequest( getCart );
    handleSuccess( req, res, cartResponse );
  } catch ( cartError ) {
    handleError( req, res, cartError );
  };
};

const _getCartsBySellerStatusState = async ( req, res ) => {
  const getCart = {
    path: `/findBySellerIdAndStatusAndState/${req.params.sellerId}/${req.params.status}/${req.params.state}`,
    method: 'GET'
  };

  try {
    const cartResponse = await cartRequest( getCart );
    handleSuccess( req, res, cartResponse );
  } catch ( cartError ) {
    handleError( req, res, cartError );
  };
};

const _getCartsByCustomer = async ( req, res ) => {
  const getCart = {
    path: `/findOrdersCustomerCpf/${req.params.document}`,
    method: 'GET'
  };

  try {
    const cartResponse = await cartRequest( getCart );
    handleSuccess( req, res, cartResponse );
  } catch ( cartError ) {
    handleError( req, res, cartError );
  };
};

// Metodos referentes a item
const _newCart = async ( req, res ) => {
  const options = {
    path: `/createOrder/${req.params.sellerId}`,
    method: 'POST'
  };

  try {
    const cartResponse = await cartRequest( options );
    handleSuccess( req, res, cartResponse );
  } catch ( createCartError ) {
    handleError( req, res, createCartError );
  }

};

const _postItem = async ( req, res ) => {
  const cartId = req.params.cartId;
  const getCartOptions = {
    path: `/get/${cartId}`,
    method: 'GET'
  };

  let cartBody; // Objeto do carrinho correspondente.

  // Busca o carrinho no shoppingCart
  try {
    cartBody = await cartRequest( getCartOptions );  
  } catch ( cartErr ) {
    return handleError( req, res, cartErr );
  }

  // Valida o status do carrinho
  try {
    cartValidator( cartBody );
  } catch ( error ) {
    return handleError( req, res, error );
  }

  // Configuracao da request para adicionar item ao carrinho
  const addItemOptions = {  
    path: `/addItem/${cartId}`,
    method: 'POST',
    body: req.body
  };
  
  let reserveResponse; // Objeto que obterá a resposta do GAN
  let remotePayment;
  let orderId;
  let alreadyHasOrderId;

  // Se não trata-se de marketplace, deve realizar a reserva no GAN.
  if ( !req.body.isMarketplace && !req.body.isWeddingGift ) {

    // Se trata de uma venda com pagamento remoto
    if ( req.body.isRemotePayment || ( cartBody.deliveries[0] && cartBody.deliveries[0].isRemotePayment ) ) {
      remotePayment = true;

      if ( !cartBody.deliveries[0] ) {
        orderId = idGenerator(1, cartId);
      } else if ( cartBody.deliveries[0] && cartBody.deliveries[0].orderId ) {
        orderId = cartBody.deliveries[0].orderId;
        alreadyHasOrderId = true;
      } else {
        orderId = idGenerator(1, cartId);
      };
    };

    // Dados a serem enviados ao GAN
    const reserveOptions = {
      body: {
        header: { function: "postReservation", login: res.locals.login },
        body: {
          storeId: req.body.storeId,
          priceList: req.body.priceList,
          orderId: !remotePayment ? cartId : `AZ_${orderId}` ,
          salesMan: cartBody.salesMan,
          items: obtemItensFastShop( req.body, 1 )
        }
      },
      method: "POST"
    };

    // Realiza a reserva no GAN
    try {
      reserveResponse = await ganRequest( reserveOptions );
    } catch ( reserveError ) {
      return handleError( req, res, reserveError );
    };

    // Adiciona servicos que foram retornados do GAN ao campo additionalFields do item (cenário CJ composto por servico)
    reserveResponse.services ? req.body.additionalFields.push( reserveResponse ) : null;
  };

  // Adiciona o item ao carrinho
  try {
    const cartWithItem = await cartRequest( addItemOptions );
    if ( !remotePayment ) {
      return handleSuccess( req, res, cartWithItem );
    }
  } catch (addItemError) {
    // Dados a serem enviados ao GAN
    const deleteReservation = {
      body: {
        header: { function: "deleteReservation", login: res.locals.login },
        body: {
          storeId: req.body.storeId,
          priceList: req.body.priceList,
          orderId: !remotePayment ? cartId : `AZ_${orderId}` ,
          salesMan: cartBody.salesMan,
          items: obtemItensFastShop( req.body, 1 ),
          cartId: cartBody.id
        }
      },
      method: "POST"
    };

    ganRequest( deleteReservation );

    return handleError( req, res, addItemError );
  };

  if ( remotePayment && !alreadyHasOrderId ) {
    // Atribui o numero do pedido ao carrinho
    const setStatusOptions = {
      path: `/setStatus/${req.params.cartId}/${CART_PENDING}/P`,
      method: 'POST',
      body: {
        priceList: req.body.priceList,
        storeId: req.body.storeId,
        ffmcenter: req.body.ffmcenter,
        orderId: orderId,
        expeditionStoreId: req.body.expeditionStoreId
      }
    };
  
    try {
      const cartWithItem = await cartRequest( setStatusOptions );
      return handleSuccess( req, res, cartWithItem );
    } catch ( setStatusError ) {
      return handleError( req, res, setStatusError );
    };
  }

  // Busca o carrinho no shoppingCart
  try {

    getCartOptions.path = `/get/${cartId}`;

    cartBody = await cartRequest( getCartOptions );  
    return handleSuccess( req, res, cartBody );
  } catch ( cartErr ) {
    return handleError( req, res, cartErr );
  }


};

const _putItem = async ( req, res ) => {
  const cartId = req.params.cartId;
  const getCartOptions = {
    path: `/get/${cartId}`,
    method: 'GET'
  };

  let cartBody; // Objeto do carrinho correspondente.

  // Busca o carrinho no shoppingCart
  try {
    cartBody = await cartRequest( getCartOptions );  
  } catch ( cartErr ) {
    return handleError( req, res, cartErr );
  }

  // Valida o status do carrinho
  try {
    cartValidator( cartBody );
  } catch ( error ) {
    return handleError( req, res, error );
  }

  // Configuracao da request para alterar item do carrinho
  const updateItemOptions = {  
    path: `/updateItem/${cartId}`,
    method: 'POST',
    body: req.body
  };
  
  // Somente altera o item ao carrinho se trata-se de marketplace.
  if ( req.body.isMarketplace ) {
    try {
      const cartWithItem = await cartRequest( updateItemOptions );
      handleSuccess( req, res, cartWithItem );
    } catch (addItemError) {
      handleError( req, res, addItemError );
    };
  };
  
  // Se não trata-se de marketplace, deve realizar a reserva novamente no GAN.
  if ( !req.body.isMarketplace && !req.body.isWeddingGift ) {

    let remotePayment;
    let orderId;

    // Se trata-se de uma venda com pagamento remoto
    if ( req.body.isRemotePayment || cartBody.deliveries[0].isRemotePayment ) {
      remotePayment = true;
      orderId = cartBody.deliveries[0].orderId || idGenerator(1, cartId);
    };

    // Dados a serem enviados ao GAN
    const reserveOptions = {
      body: {
        header: { function: "postReservation", login: res.locals.login },
        body: {
          storeId: req.body.storeId,
          priceList: req.body.priceList,
          orderId: !remotePayment ? cartId : `AZ_${orderId}` ,
          salesMan: cartBody.salesMan,
          items: obtemItensFastShop( req.body, 1 )
        }
      },
      method: "POST"
    };

    // Realiza a reserva no GAN
    try {
      await ganRequest( reserveOptions );
    } catch ( reserveError ) {
      // Possui o return para parar a execucao da funcao neste ponto, caso nao realize a reserva
      return handleError( req, res, reserveError );
    };

    // Altera o item do carrinho
    try {
      const cartWithItem = await cartRequest( updateItemOptions );
      return handleSuccess( req, res, cartWithItem );
    } catch ( updateItemError ) {
      handleError( req, res, updateItemError );
    };

  }

};

const _deleteItem = async ( req, res ) => {
  const cartId = req.params.cartId;
  const sku = req.params.sku;
  const getCartOptions = {
    path: `/get/${cartId}`,
    method: 'GET'
  };

  let cartBody; // Objeto do carrinho correspondente.

  // Busca o carrinho no shoppingCart
  try {
    cartBody = await cartRequest( getCartOptions );  
  } catch ( cartErr ) {
    return handleError( req, res, cartErr );
  }

  try {
    cartValidator( cartBody );
  } catch ( error ) {
    return handleError( req, res, error );
  }

  // Configuracao da request para remover item do carrinho
  const removeItemOptions = {  
    path: `/removeItem/${cartId}/${sku}`,
    method: 'POST'
  };

  // Busca o item enviado dentro do carrinho
  let isMarketplace;
  let items = [];
  cartBody.deliveries.some( delivery => delivery.orderItems.some( item => {
    if (item.sku === sku) {
      isMarketplace = item.isMarketplace;
      items.push({
        sku: item.sku,
        quantity: parseInt( item.quantity )
      });
      return true;
    }
  }));
  
  // Somente remove o item do carrinho se trata-se de marketplace.
  if ( isMarketplace ) {
    try {
      const cartWithItem = await cartRequest( removeItemOptions );
      return handleSuccess( req, res, cartWithItem );
    } catch ( removeItemError ) {
      return handleError( req, res, removeItemError );
    };
  };
  
  // Se não trata-se de marketplace, deve realizar a reserva novamente no GAN.
  if ( !isMarketplace ) {

    let remotePayment;
    let orderId;

    // Se trata-se de uma venda com pagamento remoto
    if ( req.body.isRemotePayment || cartBody.deliveries[0].isRemotePayment ) {
      remotePayment = true;
      orderId = cartBody.deliveries[0].orderId || idGenerator(1, cartId);
    };

    // Dados a serem enviados ao GAN
    const reserveOptions = {
      body: {
        header: { function: "deleteReservation", login: res.locals.login },
        body: {
          storeId: cartBody.deliveries[0].storeId,
          priceList: cartBody.deliveries[0].priceList,
          orderId: !remotePayment ? cartId : `AZ_${orderId}`,
          salesMan: cartBody.salesMan,
          items: items,
          cartId: cartBody.id
        }
      },
      method: "POST"
    };

    // Realiza a reserva no GAN
    try {
      await ganRequest( reserveOptions );
    } catch ( reserveError ) {
      // Possui o return para parar a execucao da funcao neste ponto, caso nao apague a reserva
      return handleError( req, res, reserveError );
    };

    // Remove o item do carrinho
    try {
      const cartWithItem = await cartRequest( removeItemOptions );
      handleSuccess( req, res, cartWithItem );
    } catch ( removeItemError ) {
      handleError( req, res, removeItemError );
    };

  }

};

const _deleteService = async ( req, res ) => {
  const sku = req.params.sku;
  const parentSku = req.params.parentSku;
  const cartId = req.params.cartId;

  const cartOptions = {
    path: `/removeServiceItem/${cartId}/${sku}/${parentSku}`,
    method: 'POST'
  };

  try {
    const cartResponse = await cartRequest( cartOptions );
    handleSuccess( req, res, cartResponse );
  } catch (removeServiceError) {
    handleError( req, res, removeServiceError );
  };

};

const _workflowStatus = async ( req, res ) => {
  req.body.approvalUser = req.query.approvalUser || null;
  req.body.approvalMail = req.query.approvalMail || null;

  // Define qual será o metodo utilizado - Demanda para visualizacao no relatório do GAN qual o aprovador.
  const cartOptions = req.body.approvalUser || req.body.approvalMail ? 
      {
        path: `/setStatusWorkflow/${req.params.cartId}/${req.params.status}`,
        body: req.body || null,
        method: 'POST'
      }
    :
      {
        path: `/get/${req.params.cartId}`,
        method: 'GET'
      };

  try {
    const cartResponse = await cartRequest( cartOptions );
    handleSuccess( req, res, cartResponse );
  } catch ( workflowError ) {
    handleError( req, res, workflowError );
  }
};

// Metodos referentes a dados do carrinho
const _setCustomer = async ( req, res ) => {
  const customerOptions = {
    path: `/setCustomer/${req.params.cartId}`,
    method: 'POST',
    body: req.body
  };

  try {
    const cartWithCustomer = await cartRequest( customerOptions );
    handleSuccess( req, res, cartWithCustomer );
  } catch ( customerError ) {
    handleError( req, res, customerError );
  }

};

const _setDeliveryAddress = async ( req, res ) => {
  const setDeliveryOptions = {
    path: `/setDeliveryAddress/${req.params.cartId}`,
    method: 'POST',
    body: req.body
  };

  try {
    const cartWithDelivery = await cartRequest( setDeliveryOptions );
    handleSuccess( req, res, cartWithDelivery );
  } catch ( deliveryError ) {
    handleError( req, res, deliveryError );
  }
};

// Funcao v3 criada para realizar o cálculo da nova database levando em consideração entregas utilizando CD de outro estado
const _shippingCalculate = async ( req, res ) => {

  const marketPlaceSkus = obtemItensMarketplace(req.body.products, 0);

  // tratamento para MarkPlace
  if ( marketPlaceSkus.length > 0 ) {
    const esbVTEX = {
      path: '/shipping/v1/consultShippingRule',
      method: 'POST',
      from: "SHIPPINGVTEX",
      body: {
        consultShippingRuleRequest: {
          Shipping: {
            AccessKey: "FastShop",
            ZipCode: req.body.zipCode.replace(/-/g, ''),
            SystemSource: "APP",
            ScheduleDelivery: { 
              InventoryLocationID: req.body.storeId,
              FulfillmentCenter: req.body.products[0].marketPlaceText,
              MaxNumberAllowed: req.query.maxNumberAllowed,
              PricingTableID: req.body.priceList
            },
            Products: {
              Product: marketPlaceSkus
            }
          }
        }
      }
    };  

    let vtexResponse;

    try {
      vtexResponse = await esbRequest( esbVTEX );
    } catch ( vtexError ) {
      return handleError( req, res, vtexError );
    }
    
    vtexResponse.ProductsInventory = vtexResponse.Shipping[0].ProductsInventory.ProductInventory;
    vtexResponse.Shipping.splice(0, 1);

    vtexResponse.Shipping = vtexResponse.Shipping.filter( ( item, index, array ) => {
      return item.DeliveryNameType ? item.DeliveryNameType === array[0].DeliveryNameType ? true : false : false;
    });

    vtexResponse.Shipping.forEach( (shippingObj, index)  => {
      vtexResponse.Shipping[index].SKU = vtexResponse.ProductsInventory[index].SKU;

      // tratando atributo DaysType para daysType
      vtexResponse.Shipping[index].AvailableDate.daysType = vtexResponse.Shipping[index].AvailableDate.DaysType;
      delete vtexResponse.Shipping[index].AvailableDate.DaysType;
      
    });

    return handleSuccess( req, res, vtexResponse );

  }; 

  const fastProducts = obtemItensFastShop( req.body, 0 );


  // Este ponto foi adicionado na V3
  // IF criado para determinar nova dataBase com base no leadtime do CD de estoque para o CD de entrega
  let dataBase = moment();

  // define CD de expedicao = CD de estoque como default
  if ( !req.body.expeditionStoreId || req.body.expeditionStoreId == null ) {
    req.body.expeditionStoreId = req.body.storeId;
  };

  // garantir CD 33
  if ( req.body.expeditionStoreId === "63" ) {
    req.body.expeditionStoreId = "33";
  };
  //

  if ( req.body.storeId !== req.body.expeditionStoreId ) {

    const dataBaseOptions = {
      body: {
        header: { function: "getDatabase", login: res.locals.login },
        body: {
          storeId: req.body.storeId,
          expeditionStoreId: req.body.expeditionStoreId,
          priceList: req.body.priceList,
          dataBase: null
        }
      },
      method: "POST"
    };
  
    try {
      dataBaseResponse = await ganRequest( dataBaseOptions );
    } catch ( dataError ) {
      return handleError( req, res, dataError );
    };

    dataBase = moment( dataBaseResponse.dataBase );
  };

  const packDaysRequest = {
    body: {
      header: { function: "getPackDays", brand: process.env.MQ_BRAND_INVENTORY, login: res.locals.login },
      body: {
        storeId: req.body.storeId,
        sku: fastProducts.map( item => item.sku ),
      }
    },
    method: "POST"
  };

  let packDaysResponse;
  try {
    packDaysResponse = await ganRequest( packDaysRequest );
  } catch ( packDaysError ) {
    return handleError( req, res, packDaysError );
  }

  dataBase = dataBase.add( packDaysResponse.totalPackDays, 'days' ).toISOString();
  
  // enviar o CD de expedicao para o shipping
  fastProducts[0].cdsDisponiveis = [req.body.expeditionStoreId];

  const shippingOptions = {
    path: '/shipping/v2/simplified',
    method: 'POST',
    body: {
      header: [{
        method: 'POST',
        path: '/api/shipping/',
        id: req.body.zipCode,
        source: 'APPV'
      }],
      body: [{
        cep: req.body.zipCode,
        dataBase: dataBase,
        tipoPagamento: [req.body.paymentTerm === 'BO' ? 'BOLETO' : 'CARTAO'],
        tabVendas: req.body.priceList,
        listaProdutos: fastProducts
      }]
    }
  };

  if ( dataBase ) {
    if ( dataBase.substr( 0,10 ) === new Date().toISOString().substr( 0, 10 ) ) {
      delete shippingOptions.body.body[0].dataBase;
    };
  }

  let shippingResponse;
  try {
    shippingResponse = await shippingRequest( shippingOptions );
  } catch ( shippingError ) {
    return handleError( req, res, shippingError );
  }

  // retornar mensagem caso nao haja data disponivel
  if ( shippingResponse.entregas.length === 0 ) {
    return handleError( req, res, { message: "Nenhuma data de entrega disponível", statusCode: 400 } );
  };

  shippingResponse.dataBase = shippingResponse.dataBase.split('.')[0]; // Remove ms do ISOString() respondido pelo shipping.
  shippingResponse.entregas.forEach( entrega => {
    entrega.listaServicosLogisticos.forEach( logistico => {
      logistico.dataBase = logistico.dataBase.split('.')[0]; // Remove ms do ISOString() respondido pelo shipping.
      !/\D/g.test( logistico.valorFreteCusto.toString() ) ? 
        logistico.valorFreteCusto *= 100 // Normaliza os numéricos (transforma em inteiros)
      : logistico.valorFreteCusto = parseInt(logistico.valorFreteCusto.toString().replace(/\D/g, "")); // Normaliza os numéricos (transforma em inteiros)
      !/\D/g.test( logistico.valorFretePromocao.toString() ) ? 
        logistico.valorFretePromocao *= 100 // Normaliza os numéricos (transforma em inteiros)
      : logistico.valorFretePromocao = parseInt(logistico.valorFretePromocao.toString().replace(/\D/g, "")); // Normaliza os numéricos (transforma em inteiros)
      
      if ( logistico.agenda instanceof Array ) {
        logistico.agenda.forEach( agenda => {
          agenda.dataBase = agenda.dataBase.split('.')[0]; // Remove ms do ISOString() respondido pelo shipping.
        });
      };
    }); 
  });

  handleSuccess( req, res, shippingResponse );

};

const _setNormalShipping = async ( req, res ) => {

  const getCart = {
    path: `/get/${req.params.cartId}`,
    method: 'GET'
  };

  let cartResponse;

  try {
    cartResponse = await cartRequest( getCart );
  } catch ( cartError ) {
    return handleError( req, res, cartError );
  };

  if ( cartResponse.deliveries[0].isRemotePayment ) {

    const shippingOptions = {
      path: '/shipping/v2/simplified',
      method: 'POST',
      body: shippingBuilder( 'normal', req, cartResponse )
    };

    try {
      const shippingResponse = await shippingRequest( shippingOptions );
      req.body.idReserva = shippingResponse.idReserva;
    } catch ( shippingError ) {
      return handleError( req, res, shippingError );
    };
  
  };

  const setShippingOptions = {
    path: `/setShipping/${req.params.cartId}`,
    method: 'POST',
    body: req.body
  };

  try {
    const cartWithShipping = await cartRequest( setShippingOptions );
    return handleSuccess( req, res, cartWithShipping );
  } catch ( shippingError ) {
    return handleError( req, res, shippingError );
  }
};

const _setScheduledShipping = async ( req, res ) => {

  const getCart = {
    path: `/get/${req.params.cartId}`,
    method: 'GET'
  };

  let cartResponse;

  try {
    cartResponse = await cartRequest( getCart );
  } catch ( cartError ) {
    return handleError( req, res, cartError );
  };

  if ( cartResponse.deliveries[0].isRemotePayment ) {

    const shippingOptions = {
      path: '/shipping/v2/simplified',
      method: 'POST',
      body: shippingBuilder( 'scheduled', req, cartResponse )
    };

    try {
      const shippingResponse = await shippingRequest( shippingOptions );
      req.body.idReserva = shippingResponse.idReserva;
    } catch ( shippingError ) {
      return handleError( req, res, shippingError );
    };
  
  };

  const setShippingOptions = {
    path: `/setShipping/${req.params.cartId}`,
    method: 'POST',
    body: req.body
  };

  try {
    const cartWithShipping = await cartRequest( setShippingOptions );
    return handleSuccess( req, res, cartWithShipping );
  } catch ( shippingError ) {
    return handleError( req, res, shippingError );
  };
};

const _setPaymentData = async ( req, res ) => {
  const setPaymentDataOptions = {
    path: `/setPaymentData/${req.params.cartId}`,
    method: 'POST',
    body: req.body
  };

  try {
    const cartWithPayments = await cartRequest( setPaymentDataOptions );
    handleSuccess( req, res, cartWithPayments );
  } catch ( paymentError ) {
    handleError( req, res, paymentError );
  };
};

const _setM1 = async ( req, res ) => {
  const setM1 = {
    path: `/setM1/${req.params.cartId}`,
    method: 'POST',
    body: req.body
  };

  try {
    const cartWithM1 = await cartRequest( setM1 );
    handleSuccess( req, res, cartWithM1 );
  } catch ( m1Error ) {
    handleError( req, res, m1Error );
  };
};

const _setSpecifier = async ( req, res ) => {
  const setSpecifier = {
    path: `/setSpecifier/${req.params.cartId}`,
    method: 'POST',
    body: req.body
  };

  try {
    const cartWithSpecifier = await cartRequest( setSpecifier );
    handleSuccess( req, res, cartWithSpecifier );
  } catch ( specifierError ) {
    handleError( req, res, specifierError );
  };
};

const _setWedding = async ( req, res ) => {
  const setWedding = {
    path: `/setWedding/${req.params.cartId}`,
    method: 'POST',
    body: req.body
  };

  try {
    const cartWithWedding = await cartRequest( setWedding );
    handleSuccess( req, res, cartWithWedding );
  } catch ( weddingError ) {
    handleError( req, res, weddingError );
  };
};

const _setWeddingMessage = async ( req, res ) => {
  const setWeddingMessage = {
    path: `/setWeddingMessage/${req.params.cartId}`,
    method: 'POST',
    body: req.body
  };

  try {
    const cartWithWeddingMessage = await cartRequest( setWeddingMessage );
    handleSuccess( req, res, cartWithWeddingMessage );
  } catch ( weddingMessageError ) {
    handleError( req, res, weddingMessageError );
  };
};

// Finalizacao do carrinho
const _postCheckout = async ( req, res ) => {
  const cartId = req.params.cartId || req.body.shoppingCartId;
  const cartBody = res.locals.cartBody;

  // Obtém itens para realizar a reserva
  const fastShopItems = obtemItensFastShop( cartBody.deliveries[0].orderItems, 1 );
  const marketPlaceItems = obtemItensMarketplace( cartBody.deliveries[0].orderItems, 1 );

  // Sprint 016 - Habilitar Pagamento remoto
  let isRemotePayment;
  let orderId;

  // Se possuir mais de um item 
  if ( fastShopItems.length > 0 ) {

    // Sprint 016 - Habilitar Pagamento remoto
    fastShopItems.some( item => {
      if ( item.isRemotePayment ) {
        isRemotePayment = true;
        orderId = cartBody.deliveries[0].orderId;
        return true;
      };
    });

    if ( !fastShopItems.some( item => item.isWeddingGift ) ) {
      // Refaz a reserva dos itens no GAN antes de enviar o pedido.
      const ganReserveOptions = {
        body: {
          header: { function: "postReservation", login: res.locals.login },
          body: {
            storeId: cartBody.deliveries[0].storeId,
            priceList: cartBody.deliveries[0].priceList,
            orderId: !isRemotePayment ? cartId : `AZ_${orderId}`,
            salesMan: cartBody.salesMan,
            items: fastShopItems
          }
        },
        method: "POST"
      };
    
      try {
        await ganRequest( ganReserveOptions );
      } catch ( reserveError ) {
        return handleError( req, res, reserveError );
      };
    };
  
    // Um pedido sem endereço de entrega é tratado como retirada em loja. 
    if ( !(cartBody.deliveryAddress instanceof Object) || Object.keys( cartBody.deliveryAddress ).length === 0 ) {
      cartBody.deliveryAddress = {};
      cartBody.deliveryAddress.withdraw = true;
    };
  
    if ( !isRemotePayment ) {
      // Envia o pedido para o GAN
      const postOrderOptions = {
        body: {
          header: { function: "postOrder", brand: process.env.MQ_BRAND_ORDER, login: res.locals.login },
          body: cartBody
        },
        method: "POST",
        timeout: process.env.GAN_TIMEOUT || 5000
      };

      let ganOrder; // Objeto contendo o retorno do GAN
      try {
        ganOrder = await ganRequest( postOrderOptions );
      } catch ( postOrderError ) {
        return handleError( req, res, postOrderError );
      };
    
      // Faz o update dos itens do carrinho (Fire & Forget)
      const updateItems = [];
      ganOrder.deliveries.forEach( delivery => delivery.orderItems.forEach( item => {
        const updateOptions = {
          path: `/updateItem/${cartId}`,
          method: 'POST',
          body: item,      
        };
    
        updateItems.push( cartRequest( updateOptions ) );
      }));
    
      try {
        Promise.all( updateItems );
      } catch ( updateError ) {
        return handleError( req, res, updateError );
      };
    
      // Estava retornando um array de pedidos ao finalizar
      const getCart = {
        path: `/get/${req.params.cartId}`,
        method: 'GET'
      };
    
      try {
        const cartResponse = await cartRequest( getCart );
        return handleSuccess( req, res, cartResponse );
      } catch ( cartError ) {
        return handleError( req, res, cartError );
      };
    } else {
      // tenta fazer o CustomerSync
      try {
        await validateCustomer( cartBody );
        customerSync( cartBody );
      } catch ( error ) {
        return handleError( req, res, error );
      };

      // Envia pedido para VTEX através do Barramento (ESB)
      let submitOrderResponse;
      try { 
        submitOrderResponse = await submitOrder( cartBody, 1 );
      } catch ( submitOrderError ) {
        return handleError( req, res, submitOrderError );
      }

      const setLink = {
        path: `/setLink/${cartId}`,
        method: 'POST',
        body: { payLink: submitOrderResponse.PayLink }
      };

      try {
        const linkResponse = await cartRequest( setLink );
      } catch ( linkError ) {
        console.error( `Falha ao atribuir link ao carrinho ${cartId}. ${submitOrderResponse.PayLink}` );
      };

      const emailOptions = {
        method: 'POST',
        from: 'EMAIL',
        body: {
          idOrder: orderId
        }
      };

      try {
        adminRequest( emailOptions );
      } catch ( emailError ) {
        handleError( req, res, emailError );
      };

      const cartOptions = {
        path: `/setStatus/${req.params.cartId}/${CART_CLOSED}/${req.query.state}`,
        method: 'POST',
        body: {
          priceList: cartBody.deliveries[0].priceList,
          storeId: cartBody.deliveries[0].storeId,
          ffmcenter: cartBody.deliveries[0].ffmcenter,
          orderId: orderId,
          expeditionStoreId: cartBody.deliveries[0].expeditionStoreId
        }
      };    

      try {
        const cartBody = await cartRequest( cartOptions );
        return handleSuccess( req, res, cartBody );
      } catch ( setStatusError ) {
        return handleError( req, res, setStatusError );
      }
    };

  }

  if ( marketPlaceItems.length > 0 ) {
    
    // tenta fazer o CustomerSync
    try {
      await validateCustomer( cartBody );
      customerSync( cartBody );
    } catch ( error ) {
      return handleError( req, res, error );
    };

    // Envia pedido para VTEX através do Barramento (ESB)
    let submitOrderResponse;
    try { 
      submitOrderResponse = await submitOrder( cartBody );
    } catch ( submitOrderError ) {
      return handleError( req, res, submitOrderError );
    }

    const cartOptions = {
      path: `/setStatus/${req.params.cartId}/${CART_CLOSED}/${req.query.state}`,
      method: 'POST',
      body: {
        priceList: cartBody.deliveries[0].priceList,
        storeId: cartBody.deliveries[0].storeId,
        ffmcenter: cartBody.deliveries[0].ffmcenter,
        ffmCenterOrder: submitOrderResponse
      }
    };    

    try {
      const cartBody = await cartRequest( cartOptions );
      return handleSuccess( req, res, cartBody );
    } catch ( setStatusError ) {
      return handleError( req, res, setStatusError );
    }

  };

};

const _setStatus = async ( req, res ) => {

  // Se for cancelamento, irá remover a reserva/apagar o pedido
  if ( req.params.status === 'X' ) {
    const cartOptions = {
      path: `/get/${req.params.cartId}`,
      method: 'GET'
    };

    let cartBody; // Objeto com o conteúdo do carrinho
    try {
      cartBody = await cartRequest( cartOptions );
    } catch ( cartError ) {
      return handleError( req, res, cartError );
    }

    // Itens para remover a reserva
    const itemsToDelete = [];
    cartBody.deliveries.forEach( delivery => delivery.orderItems.forEach( item => itemsToDelete.push( { sku: item.sku, quantity: item.quantity } ) ) );

    if ( itemsToDelete.length > 0 ) {
      const ganOptions = {
        body: {
          header: { function: "deleteReservation", login: res.locals.login },
          body: {
            storeId: cartBody.deliveries[0].storeId || null,
            priceList: cartBody.deliveries[0].priceList || null,
            orderId: cartBody.id,
            salesMan: cartBody.salesMan,
            items: itemsToDelete,
            cartId: cartBody.id
          }
        },
        method: "POST"
      };
  
      // Se possuir um pedido, não deve realizar a deleção da reserva e sim a deleção do pedido.
      if ( cartBody.deliveries[0].orderId ) {
        ganOptions.body.header.function = 'deleteOrder';
        ganOptions.body.header.brand = 'APISERVICEORDER';
        ganOptions.body.body.orderId = parseInt( cartBody.deliveries[0].orderId );
      };
  
      try {
        ganRequest( ganOptions );
      } catch ( ganError ) {
        console.log( `Erro ao remover pedido/reserva. Carrinho: ${req.params.cartId}` );
      };  
    }
  }

  const cartOptions = {
    path: `/setStatus/${req.params.cartId}/${req.params.status}/${req.query.state}`,
    method: 'POST',
    body: req.body || null
  };

  try {
    const cartResponse = await cartRequest( cartOptions );
    handleSuccess( req, res, cartResponse );
  } catch ( cartError ) {
    handleError( req, res, cartError );
  };
};

module.exports = {
  getCart: _getCart
, getCartsBySeller: _getCartsBySeller
, getCartsBySellerStatus: _getCartsBySellerStatus
, getCartsBySellerStatusState: _getCartsBySellerStatusState
, getCartsByCustomer: _getCartsByCustomer

, newCart: _newCart

, postItem: _postItem
, putItem: _putItem
, deleteItem: _deleteItem
, deleteService: _deleteService

, workflowStatus: _workflowStatus
, setCustomer: _setCustomer
, setDeliveryAddress: _setDeliveryAddress
, setNormalShipping: _setNormalShipping
, setScheduledShipping: _setScheduledShipping
, setPaymentData: _setPaymentData
, setM1: _setM1
, setSpecifier: _setSpecifier
, setWedding: _setWedding
, setWeddingMessage: _setWeddingMessage

, postCheckout: _postCheckout
, setStatus: _setStatus

, shippingCalculatev3: _shippingCalculate
};