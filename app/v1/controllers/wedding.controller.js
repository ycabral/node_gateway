const { weddingRequest, adminRequest, wcsRequest, gcRequest } = require('../../services/connections/connectors')
    , { obtemItensFastShop } = require('../../services/helpers/products.handler')
    , { handleError, handleSuccess } = require('../../services/helpers/response.handler');


const _searchList = async ( req, res ) => {
  // Define página padrão como 1 caso não enviado pelo client
  const pageNumber = req.query.page || 1;
  const searchText = req.query.text;
  
  const weddingOptions = {
    path: `/weddinglist/search?text=${searchText}&page=${pageNumber}`,
    method: 'GET'
  };

  // Bypassa a busca para o NLC
  let weddingResponse;
  try {
    weddingResponse = await weddingRequest( weddingOptions );
  } catch ( weddingError ) {
    return handleError( req, res, weddingError );
  };

  // Preenche as listas antigas com o campo id = null, para as novas listas prepara as requests paralelas
  const weddingRequests = [];
  weddingResponse.forEach( wedding => {
    if ( wedding.old ) {
      wedding.id = null;
    } else if ( !wedding.old && !wedding.id ) {
      weddingRequests.push(weddingRequest({
        path: `/weddinglist/search?text=${wedding.weddingListNumber}`,
        method: 'GET'
      }));
    };
  });

  // Realiza as multiplas requests para o NLC
  const multipleWeddings = await Promise.all( weddingRequests.map( req => req.catch( err => err ) ) );
  const validResponses = multipleWeddings.filter( result => !(result instanceof Error) );
  
  // Dentro de todas as listas, busca o 
  weddingResponse.forEach( wedding => {
    if ( wedding.old ) return true;
    validResponses.some( weddingIds => 
      weddingIds[0].weddingListNumber === wedding.weddingListNumber ? wedding.id = weddingIds[0].id : false
    );
  });

  handleSuccess( req, res, weddingResponse );

};

const _getProducts = async ( req, res ) => {
  const idLista = req.query.id;
  const weddingListNumber = req.query.weddingListNumber;
  const filters = req.query.filters;
  let page = parseInt(req.query.page) || 1;

  // Caso o parâmetro id seja passado, significa que se trata de lista nova.
  // Caso seja weddingListNumber, significa que se trata de lista do ADMIN
  let weddingResponse = {};

  if ( idLista ) {

    // Monta o request para o Nova Lista
    const weddingOptions = filters ? {
        method: 'GET',
        path: `/products/${idLista}?page=${--page}&filters=${filters}`
      } : {
        method: 'GET',
        path: `/products/${idLista}?page=${--page}`
      };
    

    // Realiza o request para o Nova Lista
    try {
      weddingResponse = await weddingRequest( weddingOptions );
    } catch ( weddingListError ) {
      return handleError( req, res, weddingListError );
    };

    // Padroniza a Resposta somente com dados que o front-end utiliza
    weddingResponse.products.forEach( product => {
      delete product.id;
      delete product.installmentPrice;
      delete product.totalPrice;
      delete product.listWcsCategoryId;
      delete product.lastUpdate;
      delete product.departmentInternalId;
      delete product.wishlist;
      delete product.selected;
      delete product.categoryId;
      delete product.interestPrice;
      delete product.wcsCategoryId;
      product.name = product.shortDescription;
    });

  } else if ( weddingListNumber ) {

    const adminOptions = {
      weddingListNumber: weddingListNumber,
      body: {
        user: process.env.ADMIN_USER,
        password: process.env.ADMIN_PASSWORD
      },
      method: "POST",
      from: 'WEDDING'
    };

    let adminResponse;
    try {
      adminResponse = await adminRequest( adminOptions );
      adminResponse = { products: adminResponse.products };
    } catch ( adminError ) {
      return handleError( req, res, adminError );
    };

    // Obtém todos os itens na lista de casamento
    let fsProducts = obtemItensFastShop( adminResponse.products, 3 );

    let initialPosition;
    if ( page === 1 || page === 0 ) {
      initialPosition = 0;
    } else {
      initialPosition = page * 5;
    };

    fsProducts = fsProducts.splice( initialPosition, 10 );

    // Monta array que conterá todas as requests a serem feitas ao WCS
    const productRequests = [];
    fsProducts.forEach( product => {
      productRequests.push( wcsRequest( {
        method: 'GET',
        path: `/v1/seller/products/byPartNumber/${product}`
      }));
    });

    // Dispara todas as requests de forma assincrona.
    const wcsAllRequests = await Promise.all( productRequests.map( req => req.catch( err => err ) ) );
    const wcsValidResponses = wcsAllRequests.filter( result => !(result instanceof Error) );

    weddingResponse = { products: [] };

    wcsValidResponses.forEach( product => {
      if ( !product.errorCode ) {
        product.bought = false;
        product.marketPlace = false;
        product.priceOffer = parseFloat(product.priceOffer);
        product.priceTag = parseFloat(product.priceTag);
        delete product.isShippingAvailability;
        delete product.shippingAvailabilityText;
        delete product.parentPartNumber;
        delete product.manuals;
        delete product.images;
  
        weddingResponse.products.push( product );
      }
    });


    if ( weddingResponse.products.length <= 0 ) {
      return handleError( req, res, { message: 'Produtos não foram encontrados', statusCode: 400 } );
    };

  } else {
    return handleError( req, res, { message: 'Invalid Parameters', statusCode: 400 } );
  };
  
  // Descomentar se necessário. Irá remover o "." como separador de casa decimal dos preços
  // weddingResponse.products.forEach( product => {
  //   product.priceOffer = product.priceOffer * 100;
  //   product.priceTag = product.priceTag * 100;
  // });
  
  return handleSuccess( req, res, weddingResponse );

};

const _getBalance = async ( req, res ) => {

  const accountId = req.params.id;
  const gcOptions = {
    path: `${req.path.split('/account')[1]}`,
    method: 'GET'
  }; 

  let gcResponse; 
  try {
    gcResponse = await gcRequest( gcOptions );
  } catch ( gcError ) {
    return handleError( req, res, gcError );
  };

  const responseBody = gcResponse.data;

  responseBody.total *= 100;
  responseBody.reserved *= 100;
  responseBody.available *= 100;
  responseBody.canceled *= 100;
  responseBody.used *= 100;
  responseBody.accumuled *= 100;
  responseBody.complemented *= 100;
  responseBody.complementedCanceled *= 100;


  return handleSuccess( req, res, responseBody );
};

module.exports = {
  searchList: _searchList
, getProducts: _getProducts
, getBalance: _getBalance
};