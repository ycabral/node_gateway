const productController = require('../controllers/product.controller');
const router = require('express').Router(); // Objeto contendo todas as rotas do carrinho

// Cart GETTERS
  router
    .get('/products/bySku/:sku', productController.getBySku )
    .post('/products/chaordic/bySearchTerm', productController.getBySearchTerm )
    .get('/availability/product/:sku?', productController.getAvailiabity);


module.exports = router;