const weddingController = require('../controllers/wedding.controller');
const router = require('express').Router(); // Objeto contendo todas as rotas do carrinho

// Cart GETTERS
  router
    .get( '/wedding/search?', weddingController.searchList )
    .get( '/wedding/products?', weddingController.getProducts )
    .get( '/wedding/account/*', weddingController.getBalance );


module.exports = router;