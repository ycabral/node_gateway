/**
 * @module Middleware com todas as rotas que são usadas pelo APP.
 * @file Arquivo que importa todas as rotas. Este arquivo é importado em ../config/express.js
 * @author Yuri Cabral - ycabral@br.ibm.com
 * @date 13/05/19
 */
const cartRouter = require('./cart.routes');
const productRouter = require('./product.routes');
const weddingRouter = require('./wedding.routes');

module.exports = [cartRouter, productRouter, weddingRouter];