/**
 * @module Cart Routes Router Module
 * @file Arquivo que importa todas as rotas. Este arquivo é importado em ../config/express.js
 * @author Yuri Cabral - ycabral@br.ibm.com
 * @date 13/05/19
 */

const router = require('express').Router(); // Objeto contendo todas as rotas do carrinho
const cartController = require('../controllers/cart.controller');
const { orderValidator } = require('../../services/helpers/validators.handler');

// Cart GETTERS
  router
    .get( '/shoppingCart/get/:cartId', cartController.getCart )
    .get( '/shoppingCart/get/bySeller/:sellerId', cartController.getCartsBySeller )
    .get( '/shoppingCart/get/bySeller/:sellerId/:status', cartController.getCartsBySellerStatus )
    .get( '/shoppingCart/get/bySeller/:sellerId/:status/:state', cartController.getCartsBySellerStatusState )
    .get( '/shoppingCart/get/byCustomer/:document', cartController.getCartsByCustomer )
// Cart Item Setters
    .post( '/shoppingCart/createOrder/:sellerId', cartController.newCart )
    .post( '/shoppingCart/addItem/:cartId', cartController.postItem )
    .post( '/shoppingCart/updateItem/:cartId', cartController.putItem )
    .delete( '/shoppingCart/removeItem/:cartId/:sku', cartController.deleteItem )
    .delete( '/shoppingCart/removeService/:cartId/:sku/:parentSku', cartController.deleteService )
// Cart Setters - (Delivery, Customer, Shipping, PaymentData, ...)
    .post( '/shoppingCart/setCustomer/:cartId', cartController.setCustomer )
    .post( '/shoppingCart/setDeliveryAddress/:cartId', cartController.setDeliveryAddress )
    .post( '/shoppingCart/setShipping/normal/:cartId', cartController.setNormalShipping )
    .post( '/shoppingCart/setShipping/scheduled/:cartId', cartController.setScheduledShipping )
    .post( '/shoppingCart/setPaymentData/:cartId', cartController.setPaymentData )
    .post( '/shoppingCart/setM1/:cartId', cartController.setM1 )
    .post( '/shoppingCart/setSpecifier/:cartId', cartController.setSpecifier )
    .post( '/shoppingCart/setWeddingList/:cartId', cartController.setWedding )
    .post( '/shoppingCart/setWeddingMessage/:cartId', cartController.setWeddingMessage )
    .post( '/shoppingCart/setStatus/:cartId/:status', cartController.setStatus )
// Workflow Setter - Discount approval
    .post( '/shoppingCart/workflow/:cartId/:status', cartController.workflowStatus )
// Sends the cart to GAN
    .post( '/shoppingCart/proceedToCheckout/:cartId', orderValidator, cartController.postCheckout )

// Shipping Calculate
    .post( '/shoppingCart/shipping/v3', cartController.shippingCalculatev3 );

    // Comentário para teste do commit

module.exports = router;